<?php 

/* you may also like */
if ( !function_exists('wnet_video_slider')) {
   function wnet_video_slider() {
   
  $content = "";

	 $count = "0";
	 $args = array('post_type' => 'video', 'post_status' => 'publish', 'meta_key' => 'video_airdate', 'orderby'  => 'meta_value', 'posts_per_page'  => 9);
	 $args['meta_query'] = array(array('key' => 'video_status', 'value' => 'publish'));	
	 $my_query = new WP_Query($args); 
	 while ($my_query->have_posts()) : $my_query->the_post(); 
		 $content .= video_slider_post(get_the_ID());
		 $count++;
	 endwhile; 
 
	 if ($content) {
		//$slider = "<div class='arrow-left'><img src='" . get_bloginfo('stylesheet_directory') . "/libs/images/arrow-left.png' /></div>";
		$slider = "<div class='video-slider cf'>$content</div>";
		//$slider .= "<div class='arrow-right'><img src='" . get_bloginfo('stylesheet_directory') . "/libs/images/arrow-right.png' /></div>";	  
		   }
	    echo $slider;
	 }
}


if ( !function_exists('video_slider_post')) {
   function video_slider_post($pid) {

	
	 $permalink = wnet_get_permalink($pid);
	 
	 $title = get_the_title( $pid );
	 
	 if ( function_exists('wnet_get_image_url')) {
		 $args = array('w' => '480', 'h' => '270', 'default' => 'default-480x270.jpg');
		 $thumbnail = wnet_get_image_url($pid, $args);
	 } 
	 if (get_post_meta($pid, '_videoURL')) {
		 $videoURL = get_post_meta($pid, '_videoURL', true); 
	 }
	 if (get_post_meta($pid, '_videoImage')) {
		 $videoImage = get_post_meta($pid, '_videoImage', true); 
	 }
	 
	return "<div class='item cf'>
		<a href='$videoURL' class='wplightbox' title='$title'>
			<figure>
			   <img src='$thumbnail' alt=\"Thumbnail for: $title\">
			   <div class='txt-wrap'>
			      <div class='title'>$title</div>
			      <div class='sub-title'><i class='fa fa-play-circle-o'></i> PLAY VIDEO</div>
			   </div>
			</figure>
			
		</a>
		</div>";
	
   }
}


/* END of FILE */