<?php 

if ( !function_exists('wnet_blog_listing_post')) {
function wnet_blog_listing_post($post_id) {

	$title = get_the_title($post_id);
	$permalink = get_permalink($post_id);
	$date = get_the_date('M. j',$post_id);
	$year = get_the_date('Y',$post_id);
	
	$mobDate = "<span class='mobile-only mob-date'> <span class='pipe'>|</span> $date $year</span>";
	
	if (function_exists('wnet_excerpt_by_id')) {$dek = wnet_excerpt_by_id($post_id, 100, 'READ MORE');}
   
   if (function_exists('wnet_get_image_url')) {$thumbnail = wnet_get_image_url($post_id, array('w'=>768,'h'=>432));}
   else {$thumbnail = "";}
   
 
   $post_author = get_post_field( 'post_author', $post_id );
     $authorNAME = get_the_author( $post_author);
   
	echo "<article class='cf'>";
	
	echo "<div class='date no-mobile'><div class='circle'>$date</div></div>";
	echo "<div class='content'>";
	
		echo "<header class='entry-header'>";
		echo "<h2 class='entry-title alttxt'><a href='$permalink'>$title</a></h2>";
		
		echo "<div class='meta'>BY $authorNAME $mobDate</div>";
		
		echo "</header>";
		
		if (!empty($thumbnail)) {echo "<figure><a href='$permalink'><img src='$thumbnail'/></a></figure>";}
		
		echo "<div class='entry-content'>$dek</div>";
		
	
	echo "</div>";
	echo "</article>";

}
}
/* end of file */