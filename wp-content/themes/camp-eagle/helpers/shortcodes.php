<?php 

 // pullquote shortcode
function wnet_pullquote($atts, $content = null) {
 extract(shortcode_atts(array(
   "align" => 'right',
 ), $atts));

	
 return '<div class="pullquote ' . $align . '"><span>'.$content.'</span></div>';
}
add_shortcode("pullquote", "wnet_pullquote");  

/* end of file */