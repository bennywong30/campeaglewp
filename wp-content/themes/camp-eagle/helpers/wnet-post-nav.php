<?php 

if ( ! function_exists( 'wnet_post_nav' ) ) :
function wnet_post_nav() {
	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous ) {
		return;
	}

	?>
	<nav class="navigation post-navigation" role="navigation">
		<div class="nav-links cf">
			<?php
			if ( is_attachment() ) :
				previous_post_link( '%link', __( '<span class="meta-nav">Published In:</span> %title', 'twentyfourteen' ) );
			else :
				previous_post_link( '%link', __( '<div class="nav-links-previous"><span class="nav-links-label"> <i class="fa fa-angle-left"></i> LAST POST</span> <span class="nav-links-post-title">%title</span></div>', 'twentyfourteen' ) );
				next_post_link( '%link', __( '<div class="nav-links-next"><span class="nav-links-label">NEXT POST <i class="fa fa-angle-right"></i></span> <span class="nav-links-post-title">%title</span> </div>', 'twentyfourteen' ) );
			endif;
			?>
		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

/* END of FILE */