<?php

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 1000;

//Includes any files using * as the file name
if ( ! function_exists('mvc_include_all'))
{
    function mvc_include_all($file_pattern) {
	//loops the file pattern
	foreach (glob($file_pattern) as $file) { 
	    include_once $file;
	}
    }
}

if ( ! function_exists('mvc_activate_helpers'))
{
    function mvc_activate_files($dir,$file_pattern) {
	//loops the file pattern arrays
	foreach ($file_pattern as $file) { 
	    include_once TEMPLATEPATH .'/'.$dir.'/'.$file.'.php';
	}
    }
}

/*
* To activate the files for settings and helpers
* call the file's name excluding the *.php extension and add into the arrays below
*/

### Settings in directory functions/  ###
$settings_activate = array(
	'settings', 
	'settings-post-types-taxonomies', 
	'settings-disable-search', 
	'settings-enqueue', 
	'settings-enqueue-addthis', 
	'settings-wpalchemy', 
	'settings-theme-options',
	'settings-jpg-compression',
	'settings-featured-image-admin',
	'settings-oembed',
	'settings-yoast',
);


### Helpers in directory helpers/  ###
$helpers_activate = array(
	'template-tags',
	'wnet-video-slider', 
	'wnet-blog-listing-post', 
	'wnet-template-dir', 
	'wnet-post-nav',
	'shortcodes',
	
);

//Activate the functions and helpers
mvc_activate_files('functions',$settings_activate);
mvc_activate_files('helpers',$helpers_activate);