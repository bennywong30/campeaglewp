<link rel="STYLESHEET" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<div class="postmeta content-promotion">
       
       
<fieldset>
<legend>Video Promotions</legend>
  <table width="100%" border="0">	
	 <tr>
                <?php $mb->the_field('_about_video_url'); ?>
                <th width="15%"><span class='alignright'>Video URL:</span></th><td  width="85%">
                <input type="text" class='text' size="75" name="<?php $mb->the_name(); ?>" value="<?php echo $mb->get_the_value(); ?>"/>
        </td></tr>
	
	 <tr>
	          <?php $mb->the_field('_about_video_image'); ?>
                <th><span class='alignright'>Video Image:</span></th><td>
                <input type="text" class='text' size="75" name="<?php $mb->the_name(); ?>" value="<?php echo $mb->get_the_value(); ?>"/>
        </td></tr>
		
</table>
</fieldset>

       
       
       
<fieldset>
<legend>Title/Dek Overrides</legend>
  <table width="100%" border="0">	
	 <tr>
                <?php $mb->the_field('_headline'); ?>
                <th width="15%"><span class='alignright'>Headline:</span></th><td  width="85%">
                <input type="text" class='text' size="75" name="<?php $mb->the_name(); ?>" value="<?php echo $mb->get_the_value(); ?>"/>
        </td></tr>
	
	 <tr>
	          <?php $mb->the_field('_dek'); ?>
                <th><span class='alignright'>DEK:</span></th><td>
                <input type="text" class='text' size="75" name="<?php $mb->the_name(); ?>" value="<?php echo $mb->get_the_value(); ?>"/>
        </td></tr>

       
		 <tr>
				<th><span class='alignright'>Display Author:</span></th>
				<td><?php $mb->the_field('_display_author'); ?><input type="checkbox" name="<?php $mb->the_name(); ?>" value="1"<?php $mb->the_checkbox_state('1'); ?>/> <span class="note">(displays author everywhere, you should always set post author as generic account if you're not displaying author)</span></td>
				</tr>
				
		<?php if ('blog' == get_post_type( $post )) { ?>
		<tr>
				<th><span class='alignright'>Display Author Bio:</span></th>
				<td><?php $mb->the_field('_display_author_bio'); ?><input type="checkbox" name="<?php $mb->the_name(); ?>" value="1"<?php $mb->the_checkbox_state('1'); ?>/></td>
				</tr>
		<?php } ?>		
				
				
				<tr>
				<th><span class='alignright'>Hide Post Title:</span></th>
				<td><?php $mb->the_field('_hide_title'); ?><input type="checkbox" name="<?php $mb->the_name(); ?>" value="1"<?php $mb->the_checkbox_state('1'); ?>/>  <span class="note">(single post template only)</span></td>
				</tr>
				
								
	<!-- iconography -->							
	<?php $iconography =  get_option('wnet_iconography'); 
	if ($iconography != "") {
	$iconArray = explode(",",$iconography);
	$icons = array('default') + $iconArray;
	?>
 		 <tr>
                <th>Custom Icon:</th><td>
                <?php foreach ($icons as $i => $icon): ?>
                <?php $mb->the_field('_icon'); ?>
                <label class="icon"><input type="radio" name="<?php $mb->the_name(); ?>" value="<?php if ($icon == "default") {echo ""; } else { echo $icon; } ?>"<?php echo $mb->the_radio_state($icon); ?>/> <i class="fa <?php echo $icon; ?>"></i> <?php if ($icon == 'default') {echo "default";} ?></label> 
                <?php endforeach; ?>
        </td></tr>
		<?php } ?>
	<!-- end iconography -->		
		
		
</table>
</fieldset>



</div>