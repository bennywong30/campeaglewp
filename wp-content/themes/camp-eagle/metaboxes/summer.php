<div class="postmeta">
<fieldset>
    <table width="100%" border="0">	
	    <tr>
                  <?php $mb->the_field('_ddImageURL'); ?>
                  <th width="15%"><span class='alignright'>Image URL:</span></th><td  width="85%">
                  <input type="text" class='text' size="75" name="<?php $mb->the_name(); ?>" value="<?php echo $mb->get_the_value(); ?>"/>
	    </td></tr>
          
	    <tr>
		    <?php $mb->the_field('_ddTitle'); ?>
		    <th width="15%" valign="top"><span class='alignright'>Title:</span></th><td  width="85%">
		    <input type="text" class='text' size="75" name="<?php $mb->the_name(); ?>" value="<?php echo $mb->get_the_value(); ?>"/>
	    </td></tr>
          
	    <tr>
		    <?php $mb->the_field('_ddName'); ?>
		    <th width="15%" valign="top"><span class='alignright'>Name:</span></th><td  width="85%">
		    <input type="text" class='text' size="75" name="<?php $mb->the_name(); ?>" value="<?php echo $mb->get_the_value(); ?>"/>
	    </td></tr>
          
	    <tr>
		    <?php $mb->the_field('_ddPhone'); ?>
		    <th width="15%" valign="top"><span class='alignright'>Phone:</span></th><td  width="85%">
		    <input type="text" class='text' size="75" name="<?php $mb->the_name(); ?>" value="<?php echo $mb->get_the_value(); ?>"/>
	    </td></tr>
        	
    </table>
</fieldset>
</div>