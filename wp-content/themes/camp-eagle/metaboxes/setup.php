<?php

/* if primary topic is set, also set topic taxonomy */
function save_primary_topic($post_id) {
	if ( isset( $_REQUEST['primarytopic'] ) ) {
		$contentmeta = $_REQUEST['primarytopic'];
		$primaryTopic = $contentmeta[_primary_topic];
		if ($primaryTopic != "") {wp_set_post_terms( $post_id, $primaryTopic, 'topic', true);}
	}
}
add_action( 'save_post', 'save_primary_topic' );
/* end set topic taxonomy */


function my_admin_init() {
	$themepath = get_bloginfo('stylesheet_directory');
	wp_enqueue_script('jquery');
	wp_enqueue_script('jquery-ui-core');
	wp_enqueue_script('jquery-ui-datepicker', $themepath . '/metaboxes/ui/ui.datepicker.min.js', array('jquery', 'jquery-ui-core') );
	wp_enqueue_style('jquery.ui.theme', $themepath . '/metaboxes/ui/ui-lightness/jquery-ui-1.7.3.custom.css');
}
add_action('admin_init', 'my_admin_init');
   
   
function my_admin_footer() {
   echo "<script type='text/javascript'>
   	jQuery(document).ready(function($){
		$('.datepicker').datepicker({dateFormat : 'yy-mm-dd'});
		});
		</script>";
	}
add_action('admin_footer', 'my_admin_footer');	


include_once get_stylesheet_directory() . '/wpalchemy/MetaBox.php';
include_once get_stylesheet_directory() . '/wpalchemy/MediaAccess.php';

// global styles for the meta boxes
add_action('admin_enqueue_scripts', 'wnet_metabox_style');

function wnet_metabox_style() {
	if (is_admin()) {wp_enqueue_style('wpalchemy-metabox', get_stylesheet_directory_uri() . '/metaboxes/meta.css');}
}


$carousel_metabox = new WPAlchemy_MetaBox( array(
        'id' => 'contentpromometa',
        'title' => 'Content Promotion/Layout',
         'types' => array('post','page'),
        'prefix' => '', 
		'mode' => WPALCHEMY_MODE_EXTRACT,
        'template' => get_stylesheet_directory() . '/metaboxes/content-promotion.php'
) );

$talk_metabox = new WPAlchemy_MetaBox( array(
        'id' => 'videometa',
        'title' => 'Video Meta',
         'types' => array('video'),
        'prefix' => '', 
		'mode' => WPALCHEMY_MODE_EXTRACT,
        'template' => get_stylesheet_directory() . '/metaboxes/video.php'
) );

$custom_html_metabox = new WPAlchemy_MetaBox( array(
        'id' => 'customhtmlmeta',
        'title' => 'Custom Html',
         'types' => array('post','page','blog'),
        'prefix' => '', 
		'mode' => WPALCHEMY_MODE_EXTRACT,
        'template' => get_stylesheet_directory() . '/metaboxes/custom-html.php'
) );


$talk_metabox = new WPAlchemy_MetaBox( array(
        'id' => 'summermeta',
        'title' => 'Metadata',
         'types' => array('summer'),
        'prefix' => '', 
		'mode' => WPALCHEMY_MODE_EXTRACT,
        'template' => get_stylesheet_directory() . '/metaboxes/summer.php'
) );
/* End of File */
