<div class="postmeta content-promotion">
	
<fieldset>
<legend>Custom HTML embed</legend>
  <table width="100%" border="0">	

	    <tr>
                <?php $mb->the_field('_custom_html'); ?>
                <th width="15%" valign="top"><span class='alignright'>Html:</span></th><td width="85%">
               <textarea id="transcript" name="<?php $mb->the_name(); ?>" rows="15" style="width: 100%; height: 100px;"><?php $mb->the_value(); ?></textarea>
        </td></tr>
		<tr>
			<td></td>
			<td class="postmeta-note">
				Can be displayed anywhere using shortcode <b>[custom_html]</b>.				
				
				</td>
			</tr>
        </table>
		
</fieldset>
</div>