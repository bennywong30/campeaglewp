function show(item)
{
    // menu-item-bg, menu-item-link
    var elementToShow = item + "_submenu_wrapper";
    document.getElementById(elementToShow).style.visibility = "visible";
    document.getElementById('main_submenu_wrapper').style.visibility = "visible";
    document.getElementById(elementToShow).style.display = "block";
    document.getElementById('main_submenu_wrapper').style.display = "block";
    var bgItem = "#" + item + "-item-bg";
    var linkItem =  "#" + item + "-item-link";
    console.log("BgItem name:" + bgItem);
    $(bgItem).addClass("menu-active");
    $(linkItem).addClass("active");                         
}

function hide(item)
{
    var elementToHide = item + "_submenu_wrapper";
    document.getElementById(elementToHide).style.visibility = "hidden";
    document.getElementById('main_submenu_wrapper').style.visibility = "hidden";
    document.getElementById(elementToHide).style.display = "none";
    document.getElementById('main_submenu_wrapper').style.display = "none";            
    var bgItem = "#" + item + "-item-bg";
    var linkItem =  "#" + item + "-item-link";
    console.log("BgItem name:" + bgItem);
    $(bgItem).removeClass("menu-active");
    $(linkItem).removeClass("active");     
}