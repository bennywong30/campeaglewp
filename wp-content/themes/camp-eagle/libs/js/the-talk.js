// you may also like
jQuery(document).ready(function($) {
if ($(".video-slider")[0]){
$('.video-slider').each(function( index ) {

	$(this).slick({
		infinite: true,
		arrows: true,
		prevArrow: '<button class="slick-prev arrow-left slick-arrow" style="display: block;"><i class="fa fa-angle-left"></i></button>',
                nextArrow: '<button class="slick-next arrow-right slick-arrow" style="display: block;"><i class="fa fa-angle-right"></i></button>',
		lazyLoad: 'ondemand',
		slidesToScroll: 3,
		touchThreshold: 60,
		slidesToShow: 3,
		autoplay: false,
		responsive: [
			{breakpoint: 1200, settings: {slidesToShow: 3, slidesToScroll: 3}},
			{breakpoint: 768, settings: {slidesToShow: 3, slidesToScroll: 3}},
			{breakpoint: 480, settings: {slidesToShow: 1, slidesToScroll: 1}}
		]
	});
	
});	

}
});	
// end you may also like

// custom menu dropdown
jQuery(document).ready(function($) {
	
	var nav = $( ".main-navigation .menu-item" );
	$(nav).hover(
		function(){
			var position = $(this).position();
			//$(this).addClass('active');
			$(this).children('.sub-menu').addClass('menu-active');
			$(this).children('.sub-menu').css("left", position + "-50%");
			//$(this).siblings().removeClass('active');
			$(this).siblings().children('.sub-menu').removeClass('menu-active');
			//$('.sub-menu').removeClass('menu-deactive');
		}
	);
	
	$('.main-navigation .menu-item').mouseenter(function() {
		$('.sub-menu').removeClass('menu-deactive');
	}).mouseleave(function() {
		$('.sub-menu').addClass('menu-deactive');
	});
	
	
});	
// custom menu dropdown



/* load ajax multimedia posts */
jQuery(document).ready(function($) {
var pagenum = 1;
$('body').on('click', '.ajax-load-updates button', function(e) {
	pagenum += 1;
	var maxpages = $('#ajaxupdates').data('max');
	$('.ajax-load-updates button').html('<i class="fa fa-circle-o-notch fa-spin"></i> LOADING');
	$.get( wpURL+'api/ajax/?template=ajax-updates&ajaxpage='+pagenum+'', function( data ) {
		$( "#ajaxupdates" ).append( data );
		$('.ajax-load-updates button').html('SEE MORE UPDATES');
		if(pagenum >= maxpages) {$(".ajax-load-updates").hide();}
	});		
	e.preventDefault();
});
});
/* end load ajax multimedia posts */




/* populate preview player */
jQuery(document).ready(function($) {

	$('body').on('click', '.populate-player', function(e) {
		var cove = $(this).data('cove-id');

		$('#modalvideocontent').html("<div class='video-wrap no-topbar'><iframe class='partnerPlayer' frameborder='0' marginwidth='0' marginheight='0' scrolling='no' width='100%' height='100%' src='http://video.pbs.org/widget/partnerplayer/"+cove+"/?start=0&end=0&chapterbar=false&endscreen=false&autoplay=true' allowfullscreen></iframe></div>");
		
		e.preventDefault();
	});
	// on close player, remove video.
	$(document).on('closed', '.remodal', function (e) {
		if ($("#modalvideocontent")[0]){$('#modalvideocontent').html('');}
	});

});
/* END populate preview player */