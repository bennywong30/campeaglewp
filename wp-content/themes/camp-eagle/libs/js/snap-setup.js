jQuery(document).ready(function($) {

	function copyAndBuildMenu(source, destination) {
		var menuContent = $(source).html();
		$(destination).html(menuContent);
		$(destination + ' li').addClass('cf'); 
	}
	
	/* [copy desktop menu to mobile] ------------------------------------------------------------  */
	if ($("#mobilemenu").html() == "") {
		copyAndBuildMenu('.menu-topnav-container', '#mobilemenu');
	}
	
	var snapper = new Snap({element: document.getElementById('snapwrap'), touchToDrag: false,  tapToClose: true, addBodyClasses: true});
	
		
	/* [open snap menus] --------------------------------------------------------------------- */
	$(document).on('click','.menutoggle',function(e){
		var type = $(this).data('type');
		if($('.snap-drawer-right').is(":visible")) {snapper.close('right');}
		else {snapper.open('right');}
		e.preventDefault();
	});

	
	/* [on resize hide the snap menu if it's open] --------------------------------------------------------------------- */
	$(window).resize(function(){ 
		setTimeout(function(){
			var viewportWidth = $(window).width();
			if (viewportWidth >= "769") {snapper.close('right');}
		}, 500);	
	});	

});