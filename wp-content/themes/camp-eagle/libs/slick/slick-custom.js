jQuery(function($){
	
	    $('.responsive').slick({
		// dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow: '<button class="slick-prev arrow-left slick-arrow" style="display: block;"><i class="fa fa-angle-left"></i></button>',
                nextArrow: '<button class="slick-next arrow-right slick-arrow" style="display: block;"><i class="fa fa-angle-right"></i></button>',
		responsive: [{
		    breakpoint: 1024,
		    settings: {
			slidesToShow: 1,
			slidesToScroll: 1,
			// centerMode: true,
	
		    }
	
		}, {
		    breakpoint: 800,
		    settings: {
			slidesToShow: 1,
			slidesToScroll: 2,
			dots: true,
			infinite: true,
	
		    }
	
	
		}, {
		    breakpoint: 600,
		    settings: {
			slidesToShow: 1,
			slidesToScroll: 2,
			dots: true,
			infinite: true,
			
		    }
		}, {
		    breakpoint: 480,
		    settings: {
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true,
			infinite: true,
			autoplay: true,
			autoplaySpeed: 2000,
		    }
		}]
	    });
	
	
    });