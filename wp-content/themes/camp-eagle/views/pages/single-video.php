<?php get_header(); ?>



		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the single post content template.
			?>


<main class='article'>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<div class='stage'><div class='cw'>
	<?php if (function_exists('wnetvid_render_player')) {echo wnetvid_render_player($post->ID, array('player_chrome' => false));} ?>
</div></div>

<header class='entry-header page-header cf'>
	<div class="cw touch-inset">
	<?php the_title( '<h1 class="entry-title alttxt">', '</h1>' ); ?>
	</div>
</header>

<div class='page-content cw touch-inset'>
	<div class="entry-content cf"><?php the_content();	?></div>
	
</div>




</article>
</main>



<?php 		if ( comments_open() || get_comments_number() ) {

		echo "<div class='comments-wrap'><div class='cw touch-inset'>";
				comments_template();
				echo "</div></div>";
			} ?>



			
<?php endwhile;	?>


<?php get_footer(); ?>
