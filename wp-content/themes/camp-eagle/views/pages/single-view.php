<?php 
	get_header(); 
	while ( have_posts() ) : the_post();

?>

<main class='page'>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<header class='entry-header sub-page-header sub-page-programs cf'>
	<div class="cw touch-inset">
	<?php the_title( '<h1 class="entry-title alttxt">', '</h1>' ); ?>
	</div>
</header>
<div class='page-content cw touch-inset'>
	<div class="entry-content cf">
		
		
		<?php 
			$args = array('post_type' => 'summer', 'page_id' => get_the_ID(), 'orderby'  => 'date', 'order'  => 'desc', 'showposts'  => 1);
			
			$my_query = new WP_Query($args); 
			while ($my_query->have_posts()) : $my_query->the_post();
			
			
				$permalink = get_permalink( $post->ID );
				if (get_post_meta($post->ID, '_ddImageURL')) {
					$ddImageURL = get_post_meta($post->ID, '_ddImageURL', true); 
				}
				if (get_post_meta($post->ID, '_ddTitle')) {
					$ddTitle = get_post_meta($post->ID, '_ddTitle', true); 
				}
				if (get_post_meta($post->ID, '_ddName')) {
					$ddName = get_post_meta($post->ID, '_ddName', true); 
				}
				if (get_post_meta($post->ID, '_ddPhone')) {
					$ddPhone = get_post_meta($post->ID, '_ddPhone', true); 
				}
				
			endwhile; 
			wp_reset_query();
		?>
		
		
		
		<div class="tt-cells individual-summer">
	
			<?php if($ddName != ""): ?>
				<div class="tt-cells-l">
					
					<?php the_content(); ?>
				
				</div>
				<div class="tt-cells-r">
					
					<img src="<?php echo $ddImageURL; ?>"/>
					
					<div><?php echo $ddTitle; ?></div>
					
					<p><?php echo $ddName; //#4db569 ?></p>
					<p><?php echo $ddPhone; ?></p>
					
				</div>
			<?php else: ?>
				<div class="tt-cells-m">
					
					<?php the_content(); ?>
				
				</div>
			<?php endif; ?>
		</div>
		
		
		
		<div class="clear-both"></div>
	</div>

	<div class="clear-both"></div>
</div>
</article>
</main>





<?php
			// End of the loop.
		endwhile;
		?>





<?php get_footer(); ?>