<?php 
	get_header(); 
	while ( have_posts() ) : the_post();

?>

<main class='page'>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<header class='entry-header sub-page-header sub-page-news cf'>
	<div class="cw touch-inset">
	<?php the_title( '<h1 class="entry-title alttxt">', '</h1>' ); ?>
	</div>
</header>
<div class='page-content sub-content-news cw touch-inset'>
	
	
	<?php if ( is_active_sidebar( 'sidebar1' )  ) : ?>
		<aside id="secondary" class="sidebar widget-area" role="complementary">
			<?php dynamic_sidebar( 'sidebar1' ); ?>
		</aside><!-- .sidebar .widget-area -->
	<?php endif; ?>

	
	<div class="entry-content cf"><?php the_content();	?></div>
	
	<div class="clear-both"></div>
	
</div>
</article>
</main>





<?php
			// End of the loop.
		endwhile;
		?>





<?php get_footer(); ?>