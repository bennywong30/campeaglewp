<?php 
	get_header(); 
	while ( have_posts() ) : the_post();

?>

<main class='page'>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<header class='entry-header sub-page-header sub-page-about-us cf'>
	<div class="cw touch-inset">
	<?php the_title( '<h1 class="entry-title alttxt">', '</h1>' ); ?>
	</div>
</header>
<div class='page-content cw touch-inset'>
	
	<div class="entry-content cf"><?php the_content();	?></div>

	
	<div class="tt-cells about-partner video-switcher">
	
		<div class="tt-cells-l item">
	
		<?php 
			$args = array('post_type' => 'page', 'page_id' => get_the_ID(), 'orderby'  => 'date', 'order'  => 'desc', 'showposts'  => 1);
			
			$my_query = new WP_Query($args); 
			while ($my_query->have_posts()) : $my_query->the_post();
			
			
				$permalink = get_permalink( $post->ID );
				if (get_post_meta($post->ID, '_about_video_url')) {
					$videoURL = get_post_meta($post->ID, '_about_video_url', true); 
				}
				if (get_post_meta($post->ID, '_about_video_image')) {
					$videoImage = get_post_meta($post->ID, '_about_video_image', true); 
				}
				if (get_post_meta($post->ID, '_headline')) {
					$headline = get_post_meta($post->ID, '_headline', true); 
				}
				if (get_post_meta($post->ID, '_dek')) {
					$dek = get_post_meta($post->ID, '_dek', true); 
				}

			endwhile; 
			wp_reset_query();
		?>
		
			<a href="<?php echo $videoURL; ?>" class='wplightbox' title='<?php echo $headline; ?>' data-width='1024' data-height='576'>
				<figure><img src='<?php echo $videoImage; ?>' alt='<?php echo $headline; ?>' style='opacity: 1;'></figure>
				<div class="txt-wrap">
					<div class="sub-title tt-play-button opacity"><i class="fa fa-play-circle-o"></i> PLAY VIDEO</div>
				</div>
			</a>
		
		</div>
		
		<div class="tt-cells-r">
			
			<h2><?php echo $headline; ?></h2>
			
			<p><?php echo $dek; ?></p>
			
			<a href="https://campeagle.secure.force.com/register#/registration/donate" target="_blank"><button class="tt-donate-button">DONATE</button></a>

		</div>
	
	</div>
	<div class="clear-both"></div>

</div>
</article>
</main>





<?php
			// End of the loop.
		endwhile;
		?>





<?php get_footer(); ?>