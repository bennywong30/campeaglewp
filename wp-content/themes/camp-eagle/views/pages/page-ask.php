<?php 
	get_header(); 
	while ( have_posts() ) : the_post();


?>

<main class='article'>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<header class='entry-header sub-page-header sub-page-contact cf'>
	<div class="cw touch-inset">
	<?php the_title( '<h1 class="entry-title alttxt">', '</h1>' ); ?>
	</div>
</header>
<div class='page-content cw touch-inset'>
	
	<div id="map"></div>
	<script>
	  function initMap() {
	    var camp = {lat: 29.9769435, lng: -99.9610723};
	    var map = new google.maps.Map(document.getElementById('map'), {
	      zoom: 10,
	      center: camp
	    });
	    
	    var contentString = '<div id="content">'+
		'<div id="siteNotice">'+
		'</div>'+
		'<h1 id="firstHeading" class="firstHeading">Camp Eagle</h1>'+
		'<div id="bodyContent">'+
		'<p><b>ADDRESS</b></p>' +
		'<p>6424 Hackberry Rd.</p>' +
		'<p>Rocksprings, TX 78880</p>'+
		'</div>'+
		'</div>';
	  
	    var infowindow = new google.maps.InfoWindow({
	      content: contentString
	    });
	    
	    var marker = new google.maps.Marker({
	      position: camp,
	      map: map,
	      title: 'Camp Eagle',
	      label: ""
	    });
	    
	    marker.addListener('click', function() {
		infowindow.open(map, marker);
	    });
	    
	  }
	</script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhVW3raykQj75AZWtBEHRZhsqpeQ1YTAY&callback=initMap"></script>
	
	<div class="entry-content sub-entry-content cf">

	<?php 
		the_content();
	?>
	
	
		<div class="tt-cells">
			
			<div class="tt-cells-l">
				<h2>ADDRESS</h2>
				<p>Camp Eagle</p>
				<p>6424 Hackberry Rd.</p>
				<p>Rocksprings, TX 78880</p>
			</div>
			
			
			<div class="tt-cells-r">
				<h2>CONTACT INFORMATION</h2>
				<p>Summer Phone: 830-683-3533</p>
				<p>Fall-Spring Phone: 830-683-2330</p>
				<p>Fax: (830) 683-2219</p>
				<p>info@campeagle.org</p>
			</div>	
		</div>
	</div>
</div>
</article>
</main>





<?php
			// End of the loop.
		endwhile;
		?>



<?php get_footer(); ?>