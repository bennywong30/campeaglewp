<?php 
	get_header(); 
	while ( have_posts() ) : the_post();

?>

<main class='page'>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<header class='entry-header sub-page-header sub-page-programs cf'>
	<div class="cw touch-inset">
	<?php the_title( '<h1 class="entry-title alttxt">', '</h1>' ); ?>
	</div>
</header>
<div class='page-content cw touch-inset'>
	<div class="entry-content cf">
		
		
		<?php 
			$args = array('post_type' => 'summer', 'page_id' => get_the_ID(), 'orderby'  => 'date', 'order'  => 'desc', 'showposts'  => 1);
			
			$my_query = new WP_Query($args); 
			while ($my_query->have_posts()) : $my_query->the_post();
			
			
				$permalink = get_permalink( $post->ID );
				if (get_post_meta($post->ID, '_ddImageURL')) {
					$ddImageURL = get_post_meta($post->ID, '_ddImageURL', true); 
				}
				if (get_post_meta($post->ID, '_ddTitle')) {
					$ddTitle = get_post_meta($post->ID, '_ddTitle', true); 
				}
				if (get_post_meta($post->ID, '_ddName')) {
					$ddName = get_post_meta($post->ID, '_ddName', true); 
				}
				if (get_post_meta($post->ID, '_ddPhone')) {
					$ddPhone = get_post_meta($post->ID, '_ddPhone', true); 
				}
				
			endwhile; 
			wp_reset_query();
		?>
		
		
		
		<div class="tt-cells individual-summer">
	
			<?php if($ddName != ""): ?>
				<div class="tt-cells-l">
					
					<?php the_content(); ?>
				
				</div>
				<div class="tt-cells-r">
					
					<img src="<?php echo $ddImageURL; ?>"/>
					
					<div><?php echo $ddTitle; ?></div>
					
					<p><?php echo $ddName; //#4db569 ?></p>
					<p><?php echo $ddPhone; ?></p>
					
				</div>
			<?php else: ?>
				<div class="tt-cells-m">
					
					<?php the_content(); ?>
				
				</div>
			<?php endif; ?>
		</div>
		
		
		
		<div class="clear-both"></div>
	</div>

	<div class="section-column">
		<p><a href="/summer/individuals-summer-camp-register-online/"><button class="tt-donate-button">REGISTER ONLINE</button></a></p>
		<p><a href="/individuals-summer-camp-prepare/"><button class="tt-signin-button">ALREADY REGISTERED?</button></a></p>
	</div>
	
	<div class="entry-zoninator entry-individuals">
		
		<ul>
		<?php
		
		$zone_query = z_get_zone_query( 'individuals' );
		if ( $zone_query->have_posts() ) :
			while ( $zone_query->have_posts() ) : $zone_query->the_post();
				
				echo '<li>';
				echo get_the_post_thumbnail() . '';
				echo '<div class="tt-cells"><h2>' . get_the_title() . '</h2>';
				echo '' . get_the_content() . '';
				echo '<p><a href="' . get_permalink() . '"><button class="tt-learn-more-button">Learn More</button></a></p></div>';
				echo '</li>';
				
			endwhile;
			
		endif;
		wp_reset_query();
		
		?>
	
		</ul>
	</div>
	
	<div class="clear-both"></div>


	<link rel="stylesheet" href="<?php echo get_bloginfo('stylesheet_directory'); ?>/libs/slick/slick.css" type="text/css" />
	<script type="text/javascript" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/libs/slick/slick.js"></script>
	<script type="text/javascript" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/libs/slick/slick-custom.js"></script>
	
	
	<div class="section-column green-bg">
		<p>CAMP THEMES</p>
	</div>

	<div class="clear-both"></div>
	
	<div class="entry-zoninator">
		
		<div class="slider responsive">
		<?php
		
		$zone_query = z_get_zone_query( 'themes' );
		if ( $zone_query->have_posts() ) :
			while ( $zone_query->have_posts() ) : $zone_query->the_post();
				
				echo get_the_post_thumbnail() . '';
				echo '<div class="tt-cells"><h2>' . get_the_title() . '</h2>';
				echo '' . get_the_excerpt() . '';
				echo '<p><a href="' . get_permalink() . '"><button class="tt-learn-more-button">Learn More</button></a></p></div>';
				
			endwhile;
			
		endif;
		wp_reset_query();
		
		?>
	
		</div>
	</div>
	
	<div class="clear-both"></div>
</div>
</article>
</main>





<?php
			// End of the loop.
		endwhile;
		?>





<?php get_footer(); ?>