<?php 
	get_header(); 
	while ( have_posts() ) : the_post();

?>

<main class='page'>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<header class='entry-header sub-page-header cf'>
	<div class="cw touch-inset">
	<?php the_title( '<h1 class="entry-title alttxt">', '</h1>' ); ?>
	</div>
</header>
<div class='page-content cw touch-inset'>	
	<div class="entry-content cf"><?php the_content();	?></div>
</div>
</article>
</main>





<?php
			// End of the loop.
		endwhile;
		?>





<?php get_footer(); ?>