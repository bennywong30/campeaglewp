<?php 
	get_header(); 
	while ( have_posts() ) : the_post();

?>

<main class='page'>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<header class='entry-header sub-page-header sub-page-programs cf'>
	<div class="cw touch-inset">
	<?php the_title( '<h1 class="entry-title alttxt">', '</h1>' ); ?>
	</div>
</header>
<div class='page-content cw touch-inset'>
	<div class="entry-content cf"><?php the_content();	?></div>

	<div class="entry-zoninator">
		
		<ul>
		<?php
		
		$zone_query = z_get_zone_query( 'programs' );
		if ( $zone_query->have_posts() ) :
			while ( $zone_query->have_posts() ) : $zone_query->the_post();
				
				echo '<li>';
				echo get_the_post_thumbnail() . '';
				echo '<div class="tt-cells"><h2>' . get_the_title() . '</h2>';
				echo '' . get_the_excerpt() . '';
				echo '<p><a href="' . get_permalink() . '"><button class="tt-learn-more-button">Learn More</button></a></p></div>';
				echo '</li>';
				
			endwhile;
			
		endif;
		wp_reset_query();

		?>
	
			<li>
				<img src="/wp-content/uploads/2017/01/IMG_1310-672x372.jpg"/>
				
				<div class="tt-cells">
					<h2>CONTACT INFORMATION</h2>
					<p>
						Have any questions or comments? Please contact one of our camp directors and we will respond as soon as possible.
					</p>
					<div class="tt-cells-l">
						<b>Individual Camp Director</b>
						<p>Julia Green</p>
						<p>830.683.3536</p>
					</div>
					<div class="tt-cells-r">
						<b>Group Camp Director</b>
						<p>Dave Grinsell</p>
						<p>830.683.3530</p>
					</div>
				</div>
			</li>
	
		</ul>
	</div>
	
	<div class="clear-both"></div>
	
</div>
</article>
</main>





<?php
			// End of the loop.
		endwhile;
		?>





<?php get_footer(); ?>