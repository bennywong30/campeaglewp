<?php get_header(); ?>

<div class='cw cf'>
    <div class='touch-inset'>
 
    <?php get_template_part( 'views/home/_inc', 'boxes' ); ?>
    
    <div class="clear-both"></div>
    
    <?php get_template_part( 'views/home/_inc', 'video-clips' ); ?>
    <?php get_template_part( 'views/home/_inc', 'mission' ); ?>
    <?php get_template_part( 'views/home/_inc', 'about-us' ); ?>
    <?php get_template_part( 'views/home/_inc', 'message' ); ?>
    <?php //get_template_part( 'views/home/_inc', 'updates' ); ?>
    </div>
</div>
	

<?php get_footer(); ?>
