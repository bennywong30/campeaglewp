<div class='home-about home-section cf'>

	<div class='about-l mob-100'>
		<h2>ABOUT US</h2>
		
		<?php 
		global $wnetsettings;
	
		if (!empty($wnetsettings['hp_about'])) {
			echo wpautop($wnetsettings['hp_about']);
		} 
		
		?>
	
		<a href="/about-us/"><button class='tt-more-button opacity brown'>READ MORE</button></a>
		
	</div>

</div>