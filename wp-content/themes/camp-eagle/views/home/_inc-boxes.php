<div class="home-featured home-section cf">
    
    <ul>
        <li class="feat-a">
            <a href="https://campeagle.secure.force.com/register#/registration/donate" target="_blank">
                <div class="placeholder-info">
                </div>
                <div class="info">
                        <h3>Give one week Campaign</h3>
                         <!--<p>Help us fill an entire week with scholarship campers</p>-->
                        <button class="tt-more-button blue">Donate</button>
                </div>
            </a>
        </li>
        <li class="feat-b">
            <a href="/fall-spring/community-play-days/">
                <div class="placeholder-info">
                </div>
                <div class="info">
                        <h3>Community Play Day</h3>
                        <button class="tt-more-button green">Read More</button>
                </div>
            </a>
        </li>
        <li class="feat-c">
            <a href="/fall-spring/bike-climb-weekends/">
                <div class="placeholder-info">
                </div>
                <div class="info">
                         <h3>Bike and Climb Weekends</h3>
                         <button class="tt-more-button brown">Read More</button>
                 </div>
             </a>
        </li>
        <li class="feat-d">
            <a href="/fall-spring/families/">
                <div class="placeholder-info">
                </div>
                <div class="info">
                        <h3>Fall Family Camp</h3>
                        <button class="tt-more-button red">Read More</button>
                </div>
            </a>
        </li>
    </ul>

</div>