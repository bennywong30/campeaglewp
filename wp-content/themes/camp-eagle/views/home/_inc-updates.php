<div class='home-updates home-section cf'>
	
	<!-- social media -->
	<div class='updates-l mob-100'>
		<figure>
		<img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/libs/images/social-bg.jpg">
		<div class='overlay'>
		<h3>#CampEagle</h3>
		<h4>Join the conversation<br/> and follow us:</h4>	
		<div class='icons'>
		
			<?php 
	global $wnetsettings;

	if (!empty($wnetsettings['facebook'])) {
		echo "<a href='https://www.facebook.com/".$wnetsettings['facebook']."' target='_blank'><i class='fa fa-facebook-square'></i></a>";
	} 
	if (!empty($wnetsettings['twitter'])) {
		echo "<a href='https://www.twitter.com/".$wnetsettings['twitter']."' target='_blank'><i class='fa fa-twitter-square'></i></a>";
	}
	if (!empty($wnetsettings['instagram'])) {
		echo "<a href='https://www.instagram.com/".$wnetsettings['instagram']."' target='_blank'><i class='fa fa-instagram'></i></a>";
	}
	if (!empty($wnetsettings['youtube'])) {
		echo "<a href='".$wnetsettings['youtube']."' target='_blank'><i class='fa fa-youtube-square'></i></a>";
	}
	?>
		
		</div>
		</div>
		</figure>
	</div>
	<!-- END social media -->
	
	<!-- updates -->
	<div class='updates-r mob-100'>
	<h3>UPDATES</h3>
	
	<ul>
	<?php 

$args = array('post_type' => 'post', 'orderby'  => 'date', 'order'  => 'desc', 'showposts'  => 3);
$my_query = new WP_Query($args); 

while ($my_query->have_posts()) : $my_query->the_post(); 
	$title = get_the_title( $post->ID );
	$permalink = get_permalink( $post->ID );
	$date = get_the_date($post->ID);
	echo "<li class='item'><a href='$permalink'><div class='meta'>$date</div><div class='title'>$title</div></a></li>";
endwhile; 
wp_reset_query();

?></ul>
	
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>category/updates/"><button class='tt-button'>SEE ALL UPDATES</button></a>
	
	</div>
	<!-- end updates -->
	
	
</div>
