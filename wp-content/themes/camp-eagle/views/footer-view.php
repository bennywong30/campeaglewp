<footer id="footer">
	<div class='cw touch-inset cf'>

		<div class='foot-l mob-100'>
			<ul id="menu-footer" class="footer-menu social">
			<?php 
			
				function wnet_foot_nav_wrap() {
					global $wnetsettings;
				
					$wrap  = '<ul id="menu-footer" class="footer-menu">';
						$wrap .= '<span class="touch-break">';
						if (!empty($wnetsettings['facebook'])) {
							$wrap .= "<li><a href='https://www.facebook.com/".$wnetsettings['facebook']."' target='_blank'><i class='fa fa-facebook'></i></a></li>";
						} 
						if (!empty($wnetsettings['twitter'])) {
							$wrap .= "<li><a href='https://www.twitter.com/".$wnetsettings['twitter']."' target='_blank'><i class='fa fa-twitter'></i></a></li>";
						}
						if (!empty($wnetsettings['instagram'])) {
							$wrap .= "<li><a href='https://www.instagram.com/".$wnetsettings['instagram']."' target='_blank'><i class='fa fa-instagram'></i></a></li>";
						}
						if (!empty($wnetsettings['youtube'])) {
							$wrap .= "<li><a href='".$wnetsettings['youtube']."' target='_blank'><i class='fa fa-youtube'></i></a></li>";
						}
						$wrap .= '</span>';
					$wrap .= '</ul>';
					return $wrap;
				}
			?>
			
			
			<div class='top'>
				
					<span class="foot-sub-l"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/libs/images/footer-logo.png?2" alt="Camp Eagle"/></a></span>
					
					<span class="foot-sub-l touch-break m-top">
						<?php echo wnet_foot_nav_wrap(); ?>
					</span>
					
					<span class="foot-sub-r r-top"><button class='tt-donate-button'><a href="https://campeagle.secure.force.com/register#/registration/donate">DONATE</a></button></span>
				
			</div>
			<div class='bottom'>
			<?php 
			
				wp_nav_menu( array(
				'theme_location' => 'footer', 
				'menu_class' => 'footer-menu',
				//'items_wrap' => wnet_foot_nav_wrap(),
				)); 
			?>
			</div>
		</div>
		
		<div class='foot-r mob-100'>
			<a class='tt-button-mobile' href="https://campeagle.secure.force.com/register#/registration/donate" target="_blank"><button class="tt-donate-button">DONATE</button></a>
			<div class='logo'><a href="http://www.CCCA.org/" target="_blank" rel="ECFA"><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/libs/images/sponsorship.png" alt="Camp Eagle Sponsorship"/></a></div>
		</div>
		
		<div class='foot-m mob-100'>
		&copy;<?php echo date('Y'); ?> <a href="<?php echo esc_url( home_url( '/' ) ); ?>" target="_blank"> <?php bloginfo( 'name' ); ?> - Change Forever. Now.</a> All Rights Reserved.<br/>
		</div>
		
	</div>
</footer>

</div><!-- #page -->
</div><!-- #snapwrap -->

<?php wp_footer(); ?>


<?php 
	/*: This is where the preview videos are loaded to when the preview buttons are clicked. */ 
	echo "<div class='remodal video' data-remodal-id='modalvideo' data-remodal-options='hashTracking: false'>
	<div class='video-close cf'>
	<div data-remodal-action='close' class='remodal-close'><i class='fa fa-times'></i></div>
	</div>
	<div id='modalvideocontent'></div>
	</div>";
?>

</body>
</html>
