<?php global $wnetsettings; ?>
<header id="masthead" class='home parallax-window'>
	
	<div class='cw cf cw-header cw-header-menu'>
		
		<?php get_template_part( 'views/_inc', 'nav' ); ?>
		
	</div><!-- cw-header -->
	
	<div class='cw cf cw-header-parallax'>
		

	
		<div class='home-question touch-inset cf'>
		<?php 
			$args = array('post_type' => 'video', 'orderby'  => 'date', 'order'  => 'desc', 'showposts'  => 1);
			$my_query = new WP_Query($args); 
			while ($my_query->have_posts()) : $my_query->the_post(); 
				$permalink = get_permalink( $post->ID );
				$videoURL = get_post_meta($post->ID, '_videoURL', true);
				if (get_post_meta($post->ID, '_videoURL')) {
					$videoURL = get_post_meta($post->ID, '_videoURL', true); 
				}
				if (get_post_meta($post->ID, '_message')) {
					$message = get_post_meta($post->ID, '_message', true); 
				}
				echo "<div class='line1'></div><div class='line2 alttxt'>$message</div><a href='$videoURL' class='wplightbox' title='Camp Eagle' data-width='1024' data-height='576'><button class='tt-play-button opacity'><i class='fa fa-play-circle-o'></i> PLAY VIDEO</button></a>";
			endwhile; 
			wp_reset_query();
		?>
		</div>
		<img src="<?php if (!empty($wnetsettings['hp_background'])) {echo $wnetsettings['hp_background'];} ?>" />
		     
	</div><!-- cw-header -->
		
	
</header>

