<div class='touch-inset'>
        <div class='logo-nav cf'>
                <a href="#" class="menutoggle mobile-only" data-type='mobile'><i class="fa fa-bars"></i></a>
                
                <?php
                
                global $wnetsettings;
				
                        if (!empty($wnetsettings['logo'])) : ?>
                        
                                <div class='logo'><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo $wnetsettings['logo']; ?>" alt="<?php bloginfo( 'name' ); ?>"/></a></div>
                        
                <?php endif; ?>
                
                
                <nav id="site-navigation" class="main-navigation no-mobile" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
                        <ul class='ex-links'>
                                <li><a href="/about-us/">ABOUT US</a></li>
                                <li><a href="http://glorieta.bashdog.com/">GLORIETA CAMPS</a></li>
                                <li><a href="http://blackdiamond.bashdog.com/">BLACK DIAMOND CAMPS</a></li>
                        </ul>
                        <ul class='ex-btns'>
                                <li class='tt-signin-button'><a href="https://campeagle.secure.force.com/reg2_login">SIGN IN</a></li>
                                <li class='tt-donate-button'><a href="https://campeagle.secure.force.com/register#/registration/donate">DONATE</a></li>
                        </ul>
                </nav><!-- .main-navigation -->
        </div>
        
        <div class='clear-both'></div>
        
        <div class='links-nav cf'>
                <nav id="site-navigation" class="main-navigation no-mobile" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
                <?php wp_nav_menu( array('theme_location' => 'primary','menu_class' => 'primary-menu')); ?>
                </nav><!-- .main-navigation -->
        
        </div>	
</div><!-- touch-inset -->