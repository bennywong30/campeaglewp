<?php

	$paged = $_GET["ajaxpage"];

	$args = array(
	'post_type' => 'post',
	'orderby'  => 'date', 
	'order'  => 'desc',
	'post_status' => 'publish',
	'posts_per_page'  => '10',
	 'paged' => $paged
	 );

$my_query = new WP_Query($args); 
while ($my_query->have_posts()) : $my_query->the_post(); 
if (function_exists('wnet_blog_listing_post')) {wnet_blog_listing_post(get_the_ID());}
endwhile; 