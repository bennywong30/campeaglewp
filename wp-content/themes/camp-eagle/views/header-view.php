<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<!--<link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Montserrat">-->
	<title><?php wp_title(); ?></title>
	<?php wp_head(); ?>
	

</head>

<body <?php body_class(); ?>>

<?php get_template_part( 'views/_inc', 'menu-snap' ); ?>


<div id="snapwrap" class="snap-content">
<div id="page" class="site">
<?php 
	if (is_home()) {get_template_part( 'views/_inc', 'masthead-home');} 
	else {get_template_part( 'views/_inc', 'masthead');}
?>







