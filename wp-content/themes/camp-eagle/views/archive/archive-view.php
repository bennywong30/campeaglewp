<?php get_header(); ?>


	
	
<div id='archive' class='cf'>
<main id="main" class="site-main" role="main">
	<?php if ( have_posts() ) : ?>
	<header class="archive-header">
	<div class='cw touch-inset cf'>
				<?php
					echo '<h1 class="page-title">';
					single_cat_title();
					echo '</h1>';
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
	</div>	
	</header>
			
			
<div class='cw touch-inset cf'>
<div class='archive-posts'>



			<?php
			
			 $max = $wp_query->max_num_pages;
			
			echo "<div id='ajaxupdates' data-max='$max'>";
			while ( have_posts() ) : the_post();
				if (function_exists('wnet_blog_listing_post')) {wnet_blog_listing_post(get_the_ID());}
			endwhile;
			echo "</div>";
			
			
			 
	if ($max > '1') {
		echo "<div class='ajax-load ajax-load-updates ajax-button'><button>SEE MORE UPDATES</button></div>";
		// for non-js readers.
		echo "<noscript>";
		the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'twentysixteen' ),
				'next_text'          => __( 'Next page', 'twentysixteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
			) );
		echo "</noscript>";
	}
				
	
	
		endif;
		?>	
			
			

</div>
</div>
</main>
</div>



<?php get_footer(); ?>
