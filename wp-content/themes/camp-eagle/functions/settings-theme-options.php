<?php

if ( ! defined( 'ABSPATH' ) ) exit;

class theme_settings { 
  	private $dir;
 	private $file;
	private $token;
	
	

	public function __construct( $file ) {

		// Register plugin settings
		add_action( 'admin_init' , array( $this , 'register_settings' ) );

		// Add settings page to menu
		add_action( 'admin_menu' , array( $this , 'add_menu_item' ) );

		// Add settings link to plugins page
		add_filter( 'plugin_action_links_' . plugin_basename( $this->file ) , array( $this , 'add_settings_link' ) );

	}

	public function add_menu_item() {
		$hook_suffix = add_options_page( 'THEME OPTIONS' , 'Theme Options' , 'manage_options' , 'theme_settings' ,  array( $this , 'settings_page' ) );
	}

	public function add_settings_link( $links ) {
		$settings_link = '<a href="options-general.php?page=theme_settings">Settings</a>';
  		array_push( $links, $settings_link );
  		return $links;
	}



	public function register_settings() {
	register_setting( 'theme_settings', 'themesettings');
	
	add_settings_section('logo', 'Branding', '', 'themesettings');
	add_settings_section('api', 'API KEYS', '', 'themesettings');
	add_settings_section('social', 'Social Media', '', 'themesettings');
	add_settings_section('homepage', 'Homepage', '', 'themesettings');

	add_settings_field( 'logo', 'Logo', array( $this, 'settings_field'), 'themesettings', 'logo', array('setting' => 'themesettings', 'field' => 'logo', 'label' => '', 'class' => 'regular-text') );
	

	add_settings_field( 'add_this', 'Addthis', array( $this, 'settings_field'), 'themesettings', 'api', array('setting' => 'themesettings', 'field' => 'add_this', 'label' => '', 'class' => 'regular-text') );
	
	
	add_settings_field( 'facebook', 'Facebook Username', array( $this, 'settings_field'), 'themesettings', 'social', array('setting' => 'themesettings', 'field' => 'facebook', 'label' => '', 'class' => 'regular-text') );
	add_settings_field( 'twitter', 'Twitter Username', array( $this, 'settings_field'), 'themesettings', 'social', array('setting' => 'themesettings', 'field' => 'twitter', 'label' => '', 'class' => 'regular-text') );
	add_settings_field( 'instagram', 'Instagram Username', array( $this, 'settings_field'), 'themesettings', 'social', array('setting' => 'themesettings', 'field' => 'instagram', 'label' => '', 'class' => 'regular-text') );
	add_settings_field( 'youtube', 'Youtube URL', array( $this, 'settings_field'), 'themesettings', 'social', array('setting' => 'themesettings', 'field' => 'youtube', 'label' => '', 'class' => 'regular-text') );


	add_settings_field( 'hp_background', 'Background Img', array( $this, 'settings_field'), 'themesettings', 'homepage', array('setting' => 'themesettings', 'field' => 'hp_background', 'label' => '', 'class' => 'regular-text') );
	add_settings_field( 'hp_about', 'About Dek', array( $this, 'settings_field'), 'themesettings', 'homepage', array('setting' => 'themesettings', 'field' => 'hp_about', 'type' => 'textarea', 'label' => '', 'class' => 'regular-text') );
	add_settings_field( 'hp_mission', 'Mission Dek', array( $this, 'settings_field'), 'themesettings', 'homepage', array('setting' => 'themesettings', 'field' => 'hp_mission', 'type' => 'textarea', 'label' => '', 'class' => 'regular-text') );
	add_settings_field( 'hp_message', 'Message Dek', array( $this, 'settings_field'), 'themesettings', 'homepage', array('setting' => 'themesettings', 'field' => 'hp_message', 'type' => 'textarea', 'label' => '', 'class' => 'regular-text') );


}



	public function settings_field( $args ) {
    // This is the default processor that will handle most input fields.  Because it accepts a class, it can be styled or even have jQuery things (like a calendar picker) integrated in it.  Pass in a 'default' argument only if you want a non-empty default value.
    $settingname = esc_attr( $args['setting'] );
    $setting = get_option($settingname);
    $field = esc_attr( $args['field'] );
    $label = esc_attr( $args['label'] );
    $class = esc_attr( $args['class'] );
    $type = ($args['type'] ? esc_attr( $args['type'] ) : 'text' );
    $options = (is_array($args['options']) ? $args['options'] : array('true', 'false') );
    $default = ($args['default'] ? esc_attr( $args['default'] ) : '' );
    switch ($type) {
      case "checkbox":
        // dont set a default for checkboxes
        $value = $setting[$field];
        $values = ( is_array($value) ? $values = $value : array($value) );
        foreach($options as $option) {
          // each option can be an array but doesn't have to be
          if (! is_array($option)) {
            $option_label = $option;
            $option_value = $option;
          } else {
            $option_label = (isset($option[label]) ? esc_attr($option[label]) : $option[0]);
            $option_value = (isset($option[value]) ? esc_attr($option[value]) : $option[0]);
          }
          $checked = in_array($option_value, $values) ? 'checked="checked"' : '';
          echo '<span class="' . $class . '"><input type="checkbox" name="' . $settingname . '[' . $field . ']" id="' . $settingname . '[' . $field . ']" value="' . $option_value . '" ' . $checked . ' />&nbsp;' . $option_label . ' </span> &nbsp; ';
        }
        echo '<label for="' . $field . '"><p class="description">' . $label . '</p></label>'; 
        break; 
		
	case "textarea":
       $settingname = esc_attr( $args['setting'] );
    $setting = get_option($settingname);
    $field = esc_attr( $args['field'] );
    $label = esc_attr( $args['label'] );
   	if (isset($args['class'])) {$class = esc_attr( $args['class'] );}
	else {$class = "";}
	
    $value = $setting[$field];
    echo '<textarea type="text" name="' . $settingname . '[' . $field . ']" id="' . $settingname . '[' . $field . ']" class="' . $class . '" >' . $value . '</textarea><p class="description">' . $label . '</p>';
        break; 	
		
		
		
      default:
        // any case other than selects, radios, checkboxes, or textareas formats like a text input
        $value = (($setting[$field] && strlen(trim($setting[$field]))) ? $setting[$field] : $default);
        echo '<input type="' . $type . '" name="' . $settingname . '[' . $field . ']" id="' . $settingname . '[' . $field . ']" class="' . $class . '" value="' . $value . '" /><p class="description">' . $label . '</p>';
    }
	}

	public function settings_page() {
    if (!current_user_can('manage_options')) {
      wp_die( __('You do not have sufficient permissions to access this page.') );
    }

    ?>
    <div class="wrap">
	<style>
		h3 {border-bottom: 1px solid #000; text-transform: uppercase;}
		textarea {width: 80%; height: 100px;}
		</style>
	
      <h2>Theme Options</h2>

      <form action="options.php" method="POST">
        <?php settings_fields( 'theme_settings' ); ?>
        <?php do_settings_sections( 'themesettings' ); ?>
        <?php submit_button(); ?>
      </form>
    </div>
    <?php
  }
}

$plugin_settings_obj = new theme_settings( __FILE__ ); 
