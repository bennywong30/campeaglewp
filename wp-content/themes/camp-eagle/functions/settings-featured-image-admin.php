<?php 

add_filter('manage_posts_columns', 'wnet_posts_columns_img', 5);
add_action('manage_posts_custom_column', 'wnet_posts_custom_columns_img', 5, 2);

function wnet_posts_columns_img($defaults){
    $defaults['riv_post_thumbs'] = __('Thumbnail');
    return $defaults;
}

function wnet_posts_custom_columns_img($column_name, $id){
        if($column_name === 'riv_post_thumbs'){
        echo the_post_thumbnail(array('480','270'), array( 'style' => 'max-width: 100px; height: auto;' ));
    }
}

?>