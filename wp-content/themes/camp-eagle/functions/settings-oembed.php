<?php 

add_filter('embed_oembed_html', 'my_embed_oembed_html', 99, 4);
function my_embed_oembed_html($html, $url, $attr, $post_id) {
	$pos1 = strpos($html, 'youtube.com');
	$pos2 = strpos($html, 'vimeo.com');
	if ($pos1 === false && $pos2 === false) { return $html;}
	else {return '<p class="embed-youtube">' . $html . '</p>';}
}

/* end of file */