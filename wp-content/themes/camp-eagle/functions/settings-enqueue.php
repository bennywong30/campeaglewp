<?php 

function wnet_scripts() {
	global $wnetsettings; 
	
wp_enqueue_script('jquery');

// enqueue scripts.
wp_enqueue_script('snap', '//cdnjs.cloudflare.com/ajax/libs/snap.js/1.9.3/snap.min.js', array('jquery'), '1.0', true);
wp_enqueue_script('slick.min.js', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js', array('jquery'), '1.5.9', true);
wp_enqueue_script('parallax', '//cdnjs.cloudflare.com/ajax/libs/parallax.js/1.4.2/parallax.min.js', array('jquery'), '1.0', false);
wp_enqueue_script('remodal', '//cdnjs.cloudflare.com/ajax/libs/remodal/1.0.6/remodal.min.js', array('jquery'), '1.0', true);


//wp_enqueue_script('jquery.details', '//cdnjs.cloudflare.com/ajax/libs/jquery-details/0.1.0/jquery.details.min.js', array('jquery'), '1.0', true);


wp_enqueue_script('snap-setup', get_bloginfo('stylesheet_directory') . '/libs/js/snap-setup.js', array('jquery'), '1.0', true);
wp_enqueue_script('the-talk', get_bloginfo('stylesheet_directory') . '/libs/js/the-talk.js', array('jquery'), '1.0.1', true);
//wp_enqueue_script('site-sliders', get_bloginfo('stylesheet_directory') . '/libs/js/app/site-sliders.js', array('jquery'), '2.02', true);

// enqueue styles.
wp_enqueue_style('site-main', get_bloginfo('stylesheet_directory') . '/libs/css/main.css', '', '1.3289', 'all');
wp_enqueue_style('google-fonts', '//fonts.googleapis.com/css?family=Dosis:400,600,700|Gurajada', '', '1.0', 'all');
wp_enqueue_style('font-awesome', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css', '', '1.0', 'all');    	


}
add_action( 'wp_enqueue_scripts', 'wnet_scripts' );


/* ajax url pathing */
function wnet_ajax_variables() {
    echo "<script>var wpURL = '". get_bloginfo('wpurl')."/'; var themePath = '". get_bloginfo('stylesheet_directory')."/';</script>";
}
add_action('wp_head', 'wnet_ajax_variables');