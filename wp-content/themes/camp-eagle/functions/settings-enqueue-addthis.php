<?php 

function wnet_addthis_scripts() {
global $wnetsettings; 
	
	
if (!empty($wnetsettings['add_this'])) {

	wp_enqueue_script('addthis', '//s7.addthis.com/js/300/addthis_widget.js#pubid=' . $wnetsettings['add_this'], array('jquery'), '1.0', true);
	
	$twitter = ""; $addthis_ga = "";
	
	if (!empty($wnetsettings['twitter'])) {$twitter = $wnetsettings['twitter'];}
	if (!empty($wnetsettings['add_this_ga'])) {$addthis_ga = $wnetsettings['add_this_ga'];}
	
	
	$addthis_config = array(
		'data_track_addressbar' => false,
		'data_track_clickback' => true,
		'data_ga_property' => $addthis_ga,
		'data_ga_social' => true
	);
	wp_localize_script( 'addthis', 'addthis_config', $addthis_config );

	if (!empty($_GET['from_name'])) {
		$name = $_GET['from_name'];
		$message = $_GET['message'];
		$twitterTEXT = $name . ' asks: ' . $message;
	}
	else {$twitterTEXT = "";}
	
	$addthis_share = array(
		'passthrough' => array(
			'twitter' => array ('via' => $twitter, 'text' => $twitterTEXT),
		),
		'url_transforms' => array(
			'shorten' => array ('twitter' => 'bitly'),
		),
		'shortners' => array('bitly' => '')
	);
	wp_localize_script( 'addthis', 'addthis_share', $addthis_share );
}	


}
add_action( 'wp_enqueue_scripts', 'wnet_addthis_scripts' );

function addthis_add_async_attribute($tag, $handle) {
	// here we add the async flag to the disqus count script.
    if ( 'addthis' !== $handle ) {return $tag;}
	else {return str_replace( " src", " async src", $tag );}
}
add_filter('script_loader_tag',  'addthis_add_async_attribute', 10, 2);