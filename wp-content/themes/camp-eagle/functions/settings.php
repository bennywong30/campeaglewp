<?php 

global $wnetsettings;
$wnetsettings = get_option('themesettings');



function disable_kses_content() {
remove_filter('content_save_pre', 'wp_filter_post_kses');
}
add_action('init','disable_kses_content',20);


if ( ! function_exists( 'wnet_setup' ) ) :
function wnet_setup() {

	// Add RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// Enable support for Post Thumbnails, and declare two sizes.
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 672, 372, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary'   => __( 'Top Menu', 'twentyfourteen' ),
		'footer' => __( 'Footer Menu', 'twentyfourteen' ),
	) );

	// Switch default core markup for search form, comment form, and comments to output valid HTML5.
	add_theme_support( 'html5', array('search-form', 'comment-form', 'comment-list'));
 
	//add_theme_support( 'post-formats', array('aside', 'video', 'gallery', 'image', 'link', 'chat', 'audio', 'quote' ));
	
	/* bs: were using 'image' for info-graphics  & we're using 'link' for interactives */
	
	
}
endif; // twentyfourteen_setup
add_action( 'after_setup_theme', 'wnet_setup' );

// Register default widget areas. 
function wnet_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Primary Sidebar', 'twentyfourteen' ),
		'id'            => 'sidebar1',
		'description'   => __( 'Main sidebar that appears in the right column.', 'twentyfourteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'wnet_widgets_init' );


/* end of file */
