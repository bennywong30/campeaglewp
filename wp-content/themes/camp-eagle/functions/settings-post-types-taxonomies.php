<?php 

/* register taxonomies */
function wnet_create_custom_taxonomies() {

/*
	if (!taxonomy_exists('topic')) {
	register_taxonomy(
		'topic',
		array('post','blog','episodes'),
		array(
			'label' => __('Topics'),
			'hierarchical' => true,
			'public' => true,
			'show_admin_column' => true,
			'query_var' => 'topic',
			'rewrite' => array('slug' => 'topic')
            ));
    }
*/
	
}

add_action( 'init', 'wnet_create_custom_taxonomies' );
/* END register taxonomies */


/* register post types */
function wnet_create_post_types() {

    register_post_type('video', array(
        'labels' => array(
            'name' => __('Video'),
            'singular_name' => __('Video'),
            'search_items' => __('Search Videos'),
            'add_new_item' => __('Add New Video'),
            'edit_item' => __('Edit Video')
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'video'),
        'query_var' => true,
        'exclude_from_search' => false,
        'menu_position' => 5,
		'menu_icon' => 'dashicons-video-alt2',		
        'supports' => array('title','editor','excerpt','thumbnail','comments'),
        'taxonomies' => array('post_tag')
    ));
    register_post_type('summer', array(
        'labels' => array(
            'name' => __('Summer'),
            'singular_name' => __('Summer'),
            'search_items' => __('Search Summer'),
            'add_new_item' => __('Add New Summer'),
            'edit_item' => __('Edit Summer')
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'summer'),
        'query_var' => true,
        'exclude_from_search' => false,
        'menu_position' => 5,
	'menu_icon' => 'dashicons-megaphone',		
        'supports' => array('title','editor','excerpt','thumbnail','comments'),
        'taxonomies' => array('post_tag')
    ));
    register_post_type('fall-spring', array(
        'labels' => array(
            'name' => __('Fall Spring'),
            'singular_name' => __('Fall Spring'),
            'search_items' => __('Search Fall Spring'),
            'add_new_item' => __('Add New Fall Spring'),
            'edit_item' => __('Edit Fall Spring')
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'fall-spring'),
        'query_var' => true,
        'exclude_from_search' => false,
        'menu_position' => 5,
	'menu_icon' => 'dashicons-megaphone',		
        'supports' => array('title','editor','excerpt','thumbnail','comments'),
        'taxonomies' => array('post_tag')
    ));
    register_post_type('staff', array(
        'labels' => array(
            'name' => __('Staff'),
            'singular_name' => __('Staff'),
            'search_items' => __('Search Staff'),
            'add_new_item' => __('Add New Staff'),
            'edit_item' => __('Edit Staff')
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'staff'),
        'query_var' => true,
        'exclude_from_search' => false,
        'menu_position' => 5,
	'menu_icon' => 'dashicons-megaphone',		
        'supports' => array('title','editor','excerpt','thumbnail','comments'),
        'taxonomies' => array('post_tag')
    ));

}
add_action('init', 'wnet_create_post_types', 1);

/* END register post types */

function wnet_rewrite_flush() {
	wnet_create_post_types();
    flush_rewrite_rules();
}
register_activation_hook(__FILE__, 'wnet_rewrite_flush');



// add custom post types to zoninator
add_action( 'init', 'wnet_register_public_post_types_to_zones', 2);
function wnet_register_public_post_types_to_zones() {
	if ( !function_exists('Zoninator')) {
    $available_post_types = array_values( get_post_types( array( 'public' => true, '_builtin' => false ), 'names' ) );
    foreach ( $available_post_types as $post_type ) {
        add_post_type_support( $post_type, 'zoninator_zones' );
        register_taxonomy_for_object_type( 'zoninator_zones', $post_type );
    }
}
}
// end add custom post types to zoninator

/* End of File */