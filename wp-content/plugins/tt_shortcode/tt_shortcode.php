<?php

/**
 * Plugin Name: ThemeTrust Shortcodes Plugin
 * Plugin URI: http://themetrust.com/plugin/shortcodes
 * Description: Useful shortcodes for ThemeTrust themes
 * Version: 0.2
 * Author: Theme Trust
 * Author URI: http://themetrust.com
 */
 
 
 // Check on the existence of necessary scripts and styles and include if not registered/enqueued.
  
 function ttrust_shortcode_dependencies() {
 
 	// Use 'slideshow' handle to check if flexslider is enqueued already. If not, register and enqueue as "flexslider.js"
 
 	$tt_handle = "slideshow";
 	$tt_list = "enqueued";
 	
 	if (wp_script_is( $tt_handle, $tt_list )) {
       return;
     } else {
     
       //Register and enqueue script
       wp_register_script( 'flexslider.js', plugin_dir_url(__FILE__).'js/jquery.flexslider.js', array('jquery'), '1.8', true);
       wp_enqueue_script( 'flexslider.js' );
       
       //Register and enqueue style
	   wp_register_style('flexslider', plugin_dir_url(__FILE__).'css/flexslider.css', false, '1.8', 'all' );
       wp_enqueue_style('flexslider');
     }	

   wp_register_script( 'bootstrap.min.js', plugin_dir_url(__FILE__).'js/bootstrap.min.js', array('jquery'), '1.8', true);
   wp_enqueue_script( 'bootstrap.min.js' );
	
   wp_register_style('tt_shortcode', plugin_dir_url(__FILE__).'css/tt_shortcode.css' );
   wp_enqueue_style('tt_shortcode');
 			
 }
 
 // Handle shortcode registration automatically using the array of shortcodes within the function
 
 function register_and_unregister_shortcodes(){
 	
 	// Define all the shortcodes in an array to automate registering and unregistering 	
 	
 	$tt_shortcodes = array('button','one_third','one_third_last','two_third','two_third_last','one_half','one_half_last','one_fourth','one_fourth_last','three_fourth','three_fourth_last','slideshow','tab_group','tab','toggle_group','toggle');
 	
 	// Loop through the array, unregistering where they already exist. Functions have to be named strictly according to convention for this to pan out
 	
 	foreach($tt_shortcodes as $tt_shortcode) {
 		
 		$tt_shortfunction = "ttrustsc_".$tt_shortcode;
 		
 		if(shortcode_exists($tt_shortcode)){
	 		remove_shortcode($tt_shortcode);
	 	}
	 	 
 		add_shortcode($tt_shortcode, $tt_shortfunction);
 	}
 
 }
 
 // Button Shortcode

 function ttrustsc_button($a) {
	extract(shortcode_atts(array(
		'label' 	=> 'Button Text',
		'id' 	=> '1',
		'url'	=> '',
		'target' => '_parent',		
		'size'	=> '',
		'color'	=> '',		
		'ptag'	=> false
	), $a));
	
	$link = $url ? $url : get_permalink($id);
	$s = "";
	
	if($color) $s .= "background-color:".$color.";";
		
	
	if($ptag) :
		return  wpautop('<a href="'.$link.'" target="'.$target.'" style="'.$s.'" class="ttsc_button button '.$size.'">'.$label.'</a>');
	else :
		return '<a href="'.$link.'" target="'.$target.'" style="'.$s.'" class="ttsc_button button '.$size.'">'.$label.'</a>';
	endif;
 }

 // Column Shortcodes

 function ttrustsc_one_third( $atts, $content = null ) {
  global $shortcode_tags;
  $shortcode_keys = array_keys($shortcode_tags);
   return '<div class="ttsc_one_third column">' . do_shortcode($content) . '</div>';
 }

 function ttrustsc_one_third_last( $atts, $content = null ) {
   return '<div class="ttsc_one_third column ttsc_last">' . do_shortcode($content) . '</div><div class="clearboth"></div>';
 }

 function ttrustsc_two_third( $atts, $content = null ) {
   return '<div class="ttsc_two_third column">' . do_shortcode($content) . '</div>';
 }

 function ttrustsc_two_third_last( $atts, $content = null ) {
   return '<div class="ttsc_two_third column ttsc_last">' . do_shortcode($content) . '</div><div class="clearboth"></div>';
 }

 function ttrustsc_one_half( $atts, $content = null ) {
   return '<div class="ttsc_one_half column">' . do_shortcode($content) . '</div>';
 }

 function ttrustsc_one_half_last( $atts, $content = null ) {
   return '<div class="ttsc_one_half column ttsc_last">' . do_shortcode($content) . '</div><div class="clearboth"></div>';
 }

 function ttrustsc_one_fourth( $atts, $content = null ) {
   return '<div class="ttsc_one_fourth column">' . do_shortcode($content) . '</div>';
 }

 function ttrustsc_one_fourth_last( $atts, $content = null ) {
   return '<div class="ttsc_one_fourth column ttsc_last">' . do_shortcode($content) . '</div><div class="clearboth"></div>';
 }

 function ttrustsc_three_fourth( $atts, $content = null ) {
   return '<div class="ttsc_three_fourth column">' . do_shortcode($content) . '</div>';
 }

 function ttrustsc_three_fourth_last( $atts, $content = null ) {
   return '<div class="ttsc_three_fourth column ttsc_last">' . do_shortcode($content) . '</div><div class="clearboth"></div>';
 }

 function ttrustsc_reformat($content) {
	$new_content = '';

	/* Matches the contents and the open and closing tags */
	$pattern_full = '{(\[raw\].*?\[/raw\])}is';

	/* Matches just the contents */
	$pattern_contents = '{\[raw\](.*?)\[/raw\]}is';

	/* Divide content into pieces */
	$pieces = preg_split($pattern_full, $content, -1, PREG_SPLIT_DELIM_CAPTURE);

	/* Loop over pieces */
	foreach ($pieces as $piece) {
		/* Look for presence of the shortcode */
		if (preg_match($pattern_contents, $piece, $matches)) {

			/* Append to content (no formatting) */
			$new_content .= $matches[1];
		} else {

			/* Format and append to content */
			$new_content .= wptexturize(wpautop($piece));
		}
	}

	return $new_content;
 }

// Remove the 2 main auto-formatters
remove_filter('the_content', 'wpautop');
remove_filter('the_content', 'wptexturize');

// Before displaying for viewing, apply this function
add_filter('the_content', 'ttrustsc_reformat', 99);
add_filter('widget_text', 'ttrustsc_reformat', 99);

 // Slideshow Shortcode

 function ttrustsc_slideshow( $atts, $content = null ) {
    $content = str_replace('<br />', '', $content);
	$content = str_replace('<img', '<li><img', $content);
	$content = str_replace('/>', '/></li>', $content);
	return '<div class="flexslider clearfix primary normal"><ul class="slides">' . $content . '</ul></div>';
 }

 // Tabs -- Using Globals :/
 
 /* Usage: 
 * [tab_group nav="tabs" style="framed"]
 * [tab title="Tab 1"] Tab content. [/tab]
 * [tab title="Tab n"] Tab content. [/tab]
 * [/tab_group]
 */
 
 /* Use a global var to store the tabs array, including title, 
 * tab content (put later into $tab_contents[]) and a unique ID to target 
 * the correct pane
 */
 
 function ttrustsc_tab( $atts, $content ){
	extract(shortcode_atts(array(
		'title' => 'Tab %d'
	), $atts));
	
	$x = $GLOBALS['ttsc_tab_count'];
	$GLOBALS['ttsc_tabs'][$x] = array( 'title' => sprintf( $title, $GLOBALS['ttsc_tab_count'] ), 'content' =>  $content, 'id' => uniqid(rand(), false) );
	
	$GLOBALS['ttsc_tab_count']++;
 }
 
 function ttrustsc_tab_group($atts, $content) {	
	extract(shortcode_atts(array(
		 // Option to use tabs or pills and open or closed framing
		'nav' 	=> 'tabs',
		'style' 	=> 'framed',
	), $atts));

	$GLOBALS['ttsc_tab_count'] = 0;
	
	do_shortcode($content);
		
	if( is_array( $GLOBALS['ttsc_tabs'] ) ){
				
		// Create arrays for the foreach loop to contain the tab selectors and contents. We will pull this apart outside of the loop using implode()
		
		$i = 0;
		
		foreach( $GLOBALS['ttsc_tabs'] as $tab ){
			if($i == 0){
				$class = "active";
			} else {
				$class = "";
			}
			$tabs[] = "\n\t\t\t".'<li class="'.$class.'"><a href="#tabs-'.$tab['id'].'" data-toggle="tab" title="'.$tab['title'].'">'.$tab['title'].'</a></li>';
			$tab_contents[] = "\n\t\t\t".'<div class="tab-pane '.$class.'" id="tabs-'.$tab['id'].'">'.$tab['content'].'</div>';
			$i++;
		}
				
		$return = '<ul class="nav nav-'.$nav.'">'.implode( "\n", $tabs ).'</ul>'."\n\t\t"."\n".'<div class="tab-content">'.implode( "\n", $tab_contents ).'</div>'."\n";
	}
	return $return;
 }
 
 // Put tab activation JS in the footer
 
 function ttrustsc_footer_scripts(){
 	$script = '
 	<script type="text/javascript">
	 	 jQuery(document).ready(function ($) {
			$(\'#tabs\').tab();
		
			$(\'#accordion2\').collapse({
			  toggle: true
			});
		});	
	</script>';
	
	echo $script;
 }

 // Toggles - Logic pairs with tabs shortcodes. Exceptions to this are noted below.
 
 /* Usage: 
 * [toggle_group type="accordion"] (NOTE: undefined type defaults to normal toggle behavior)
 * [toggle title="Toggle 1"] Toggle content. [/toggle]
 * [toggle title="Toggle 1"] Toggle content. [/toggle]
 * [/toggle_group]
 */

 function ttrustsc_toggle( $atts, $content ){
	extract(shortcode_atts(array(
		'title' => 'Toggle %d'
	), $atts));
	
	$x = $GLOBALS['toggle_count'];
	$GLOBALS['toggles'][$x] = array( 'title' => sprintf( $title, $GLOBALS['toggle_count'] ), 'content' =>  $content, 'id' => uniqid(rand(), false) );
	
	$GLOBALS['toggle_count']++;
}


/* The Toggle group code creates separate code blocks for collapsable panels and accordions. It also tests user input and defaults to panels */

 function ttrustsc_toggle_group( $atts, $content ){
	extract(shortcode_atts(array(
		'type' => 'panel'
	), $atts));
	
	$GLOBALS['toggle_count'] = 0;
		
	do_shortcode($content);
	
	if(is_array( $GLOBALS['toggles'] )){
		if($type == "accordion" ){	
		
			$return = '<div class="'.$type.'" id="'.$type.'2">';
			$i = 0;
			foreach( $GLOBALS['toggles'] as $toggle ){
				if($i == 0){
					$class = " in";
				} else {
					$class = "";
				}
				
				// Tons'o' tabs in here to make this readable
				
				$return .= "\n".'<div class="'.$type.'-group">'."\n".'<div class="'.$type.'-heading">'."\n".'<h4 class="'.$type.'-title">'."\n".'<a href="#toggle-'.$toggle['id'].'" data-toggle="collapse" data-parent="#'.$type.'2">'.$toggle['title'].'</a></h4>'."\n".'</div>'."\n";
				$return .= '<div id="toggle-'.$toggle['id'].'" class="'.$type.'-body collapse'.$class.'">'."\n".'<div class="'.$type.'-inner">'.$toggle['content'].'</div>'."\n".'</div>'."\n".'</div>';
				$i++;
			}
			
			$return .= "\n".'</div>'; 
			
		}
		 
		if( $type !== "accordion" ){
		
			/* Because toggles appear in a different order from the tabs, we are going to use the foreach to accomplish more here.
			*  This first requires laying out the toggle group master div. This div decides whether it is a normal toggle or an accordion.
			*/ 
	
			$return = '<div class="'.$type.'-group" id="'.$type.'2">';
			$i = 0;
			foreach( $GLOBALS['toggles'] as $toggle ){
				if($i == 0){
					$class = " in";
				} else {
					$class = "";
				}
				$return .= "\n".'<div class="'.$type.' '.$type.'-default">'."\n".'<div class="'.$type.'-heading">'."\n".'<h4 class="'.$type.'-title">'."\n".'<a href="#toggle-'.$toggle['id'].'" data-toggle="collapse" data-parent="#'.$type.'">'.$toggle['title'].'</a></h4>'."\n".'</div>'."\n";
				$return .= '<div id="toggle-'.$toggle['id'].'" class="'.$type.'-collapse collapse'.$class.'">'."\n".'<div class="'.$type.'-body">'.$toggle['content'].'</div>'."\n".'</div>'."\n".'</div>';
				$i++;
			} 
			
			$return .= "\n".'</div>';			
		}	
	}
	return $return;
 }
	
	
	
 // Hook the shortcodes and Flexslider js into WP Initialization

 add_action('wp_footer','ttrustsc_footer_scripts');
 add_action( 'init', 'register_and_unregister_shortcodes');
 add_action( 'init', 'ttrust_shortcode_dependencies');
