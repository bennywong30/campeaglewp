<?php 



if ( ! function_exists('wnet_episode_content_callback')) {
    // Function use sitewide for pulling p2p single query. Less overhead and query for Post2Post
    function wnet_episode_content_callback($meta_key,$meta_value,$type,$limit,$order = 'p2p_id')
    { 
		global $wpdb;
	    
		$querystr = "SELECT $wpdb->p2p.p2p_from, $wpdb->p2p.p2p_to
		FROM $wpdb->p2p
		LEFT JOIN $wpdb->p2pmeta as p2pm_order on p2pm_order.p2p_id = $wpdb->p2p.p2p_id AND p2pm_order.meta_key = '_order_to'
		WHERE $wpdb->p2p.p2p_type = '%s' AND ($wpdb->p2p.p2p_from = '%d' OR $wpdb->p2p.p2p_to = '%d')
		ORDER BY p2pm_order.meta_value * 1 ASC
		LIMIT %d";
		$result = $wpdb->get_results($wpdb->prepare("$querystr", $type, $meta_value, $meta_value, $limit), OBJECT);
	    
	    
		return $result;
    }
}
	
	
if ( !function_exists('wnet_episode_content_array_callback') AND !function_exists('wnet_array_prepare_in')) {
	    
	    // Function use sitewide for pulling p2p single query. Less overhead and query for Post2Post
	    function wnet_episode_content_array_callback($meta_value,$type,$limit)
	    { 
			global $wpdb;
		    
			$querystr = "SELECT $wpdb->p2p.p2p_from, $wpdb->p2p.p2p_to
			FROM $wpdb->p2p
			WHERE $wpdb->p2p.p2p_type IN ([IN]) AND ($wpdb->p2p.p2p_from = '%d')
			ORDER BY FIELD($wpdb->p2p.p2p_type, [IN])
			LIMIT %d";
			
			$sql = $wpdb->prepare("$querystr", $meta_value, $limit);
			
			//prepare the [IN] with the array of strings
			$sql = wnet_array_prepare_in( $sql, $type );
			
			$result = $wpdb->get_results($sql, OBJECT);
			return $result;
	    }
    
	    function wnet_array_prepare_in( $sql, $vals ){
			global $wpdb;
			$not_in_count = substr_count( $sql, '[IN]' );
			
			if ( $not_in_count > 0 ){
				$args = array( str_replace( '[IN]', implode( ', ', array_fill( 0, count( $vals ), '%s' ) ), str_replace( '%', '%%', $sql ) ) );
				// This will populate ALL the [IN]
				for ( $i=0; $i < substr_count( $sql, '[IN]' ); $i++ ) {
				    $args = array_merge( $args, $vals );
				}
				$sql = call_user_func_array( array( $wpdb, 'prepare' ), array_merge( $args ) );
			}
			
			return $sql;
	    }
}	
	
if ( ! function_exists('wnet_p2p_single_id_callback')) {
    // Function use sitewide for pulling p2p single query. Less overhead and query for Post2Post
    function wnet_p2p_single_id_callback($p2p_id)
    { 
            global $wpdb;
            $querystr = "SELECT $wpdb->p2p.p2p_from, $wpdb->p2p.p2p_to
	    FROM $wpdb->p2p
	    INNER JOIN $wpdb->p2pmeta as p2pm_order on p2pm_order.p2p_id = $wpdb->p2p.p2p_id AND p2pm_order.meta_key = '_order_to'
	    WHERE $wpdb->p2p.p2p_from = '%d' OR $wpdb->p2p.p2p_to = '%d'
	    ORDER BY p2pm_order.meta_value ASC
	    LIMIT 1";
                
		
            $result = $wpdb->get_row($wpdb->prepare("$querystr",$p2p_id,$p2p_id));
            return $result;
    }
}	
	


// generic efficient function for getting post(s) with a certain p2p relationship and optionally excluding certain ids, requiring certain post type(s), and requiring certain post status(es)
if (! function_exists('wnet_p2p_get_related_content')) {
	function wnet_p2p_get_related_content ($postid, $relationship, $limit=1, $excluded=NULL, $post_types='public', $post_status='publish'){
    //sanity checks
    if (! isset($postid) || ! isset($relationship)) {
      return false;
    }

    if ($post_types == 'public') {
      $post_types = get_post_types(array(
            'exclude_from_search' => false,
            'public' => true), 'names');
    }
    if (is_array($post_types)) {
      $temparray = '';
      foreach ($post_types as $type) {
        $temparray[] = '"' . $type . '"';
      }
      $post_types = implode(',',$temparray);
    } else {
      $post_types = '"' . $post_types . '"';
    }
    if ($excluded) {
      $excluded = sprintf(" AND relid NOT IN (%s)", $excluded);
    }
    if ($limit > 20) {
      $limit = 10;
    }
		global $wpdb;
    $querystr = sprintf("
			SELECT $wpdb->posts.ID, $wpdb->posts.post_title
			FROM $wpdb->posts
      JOIN (
        (SELECT $wpdb->p2p.p2p_to AS relid FROM $wpdb->p2p WHERE $wpdb->p2p.p2p_from = %d AND $wpdb->p2p.p2p_type = '%s' %s) 
        UNION
        (SELECT $wpdb->p2p.p2p_from AS relid FROM $wpdb->p2p WHERE $wpdb->p2p.p2p_to = %d AND $wpdb->p2p.p2p_type = '%s' %s)
      ) AS p ON p.relid = $wpdb->posts.ID
      WHERE $wpdb->posts.post_type IN ( %s )
			AND $wpdb->posts.post_status IN ( '%s' )
      ORDER BY $wpdb->posts.post_date DESC LIMIT %d 
			", $postid, $relationship, $excluded, $postid, $relationship, $excluded, $post_types, $post_status, $limit);

		$result = $wpdb->get_results($querystr, OBJECT);
		return $result;
	}
}

// simple efficient generic utility function to simply return an array of posts that match a relationship
if ( ! function_exists('wnet_p2p_return_related_postid')) {
// simplified for direct single-post return
  // args -- originating post, which side the originating post is, relationship type, limit
  function wnet_p2p_return_related_postid($post_id,$source='p2p_from',$relationship='featured_video',$limit=1) {
    //sanity checks
    if (! isset($post_id)) {
      return false;
    } else {
      global $wpdb;
      $theseargs = array($post_id, $relationship, $limit);
      if ($source == 'p2p_to') {
        $querystr = $wpdb->prepare("SELECT p.p2p_from FROM $wpdb->p2p as p WHERE p.p2p_to = '%d' AND p.p2p_type = '%s' ORDER BY p.p2p_id LIMIT %d", $theseargs);
      } else {
       $querystr = $wpdb->prepare("SELECT p.p2p_to FROM $wpdb->p2p as p WHERE p.p2p_from = '%d' AND p.p2p_type = '%s' ORDER BY p.p2p_id LIMIT %d", $theseargs);
      }
      $result = $wpdb->get_results($querystr, ARRAY_N);
      return $result;
    }
  }
}



// COMMENT: Will: this function below will disappear.  Can just use the exact function above, but pass it arguments.
// READ THIS NEXT CUSTOM p2p query
if ( ! function_exists('wnet_p2p_read_this_next')) {
	// simplified for direct single-post return
	// args -- originating post, which side the originating post is, relationship type, limit
  function wnet_p2p_read_this_next($post_id,$source='p2p_from',$relationship='read_next',$limit=1) {
    //sanity checks
    if (! isset($post_id)) {
      return false;
    } else {
      global $wpdb;
      $theseargs = array($post_id, $relationship, $limit);
      if ($source == 'p2p_to') {
        $querystr = $wpdb->prepare("SELECT p.p2p_from FROM $wpdb->p2p as p WHERE p.p2p_to = '%d' AND p.p2p_type = '%s' ORDER BY p.p2p_id LIMIT %d", $theseargs);
      } else {
       $querystr = $wpdb->prepare("SELECT p.p2p_to FROM $wpdb->p2p as p WHERE p.p2p_from = '%d' AND p.p2p_type = '%s' ORDER BY p.p2p_id LIMIT %d", $theseargs);
      }
      $result = $wpdb->get_results($querystr, ARRAY_N);
      return $result;
    }
  }
}
// END READ THIS NEXT CUSTOM p2p query




//Replace p2p query bw_p2p_related_homepage_package
if (! function_exists('wnet_p2p_related_homepage_package')) {
	function wnet_p2p_related_homepage_package($pid){
		
			global $wpdb;
			
			//count if related_content_home exist - bad query
			$row_query = sprintf("SELECT count($wpdb->p2p.p2p_to) FROM $wpdb->p2p WHERE $wpdb->p2p.p2p_type = 'related_content_home' AND $wpdb->p2p.p2p_from = %d LIMIT 1", $pid);
			$row_query = $wpdb->get_var($row_query, OBJECT);

			if($row_query >= 1) {
				$querystr = sprintf("
					SELECT $wpdb->posts.ID, $wpdb->posts.post_title FROM $wpdb->posts
					INNER JOIN $wpdb->p2p AS wp_p2p ON ( $wpdb->posts.ID = $wpdb->p2p.p2p_to )
					LEFT JOIN $wpdb->p2pmeta AS p2pm_order ON ( $wpdb->p2p.p2p_id = p2pm_order.p2p_id AND p2pm_order.meta_key = '_order_from')
					WHERE $wpdb->posts.post_status = 'publish' AND (
						$wpdb->p2p.p2p_type = 'related_content_home' AND $wpdb->posts.ID = $wpdb->p2p.p2p_to AND $wpdb->p2p.p2p_from =%d)
					ORDER BY p2pm_order.meta_value ASC LIMIT 3
					 ", $pid);
			
			$result = $wpdb->get_results($wpdb->prepare("$querystr",NULL), OBJECT);
			return $result;
			}
	}
}



/* this function is only used on the bill moyers site */
if ( ! function_exists('wnet_many_episode_content_callback')) {
    // Function use sitewide for pulling p2p single query. Less overhead and query for Post2Post
    function wnet_many_episode_content_callback($type, $meta_value, $limit)
    { 
		global $wpdb;
	    
		$querystr = "SELECT wp_posts.ID, wp_posts.post_title, wp_p2p.p2p_id, wp_p2p.p2p_from, wp_p2p.p2p_to, wp_p2p.p2p_type
			FROM wp_posts  INNER JOIN wp_p2p
			INNER JOIN wp_p2pmeta ON (wp_p2p.p2p_id = wp_p2pmeta.p2p_id)
				LEFT JOIN wp_p2pmeta AS p2pm_order ON (
					wp_p2p.p2p_id = p2pm_order.p2p_id AND p2pm_order.meta_key = ''
				)
			WHERE wp_posts.post_status = 'publish' AND wp_p2p.p2p_type = '%s' AND (
					(wp_posts.ID = wp_p2p.p2p_to AND wp_p2p.p2p_from IN ('%d')) OR
					(wp_posts.ID = wp_p2p.p2p_from AND wp_p2p.p2p_to IN ('%d'))
				) AND ( (wp_p2pmeta.meta_key = 'type' AND CAST(wp_p2pmeta.meta_value AS CHAR) = 'related') )
			ORDER BY wp_p2p.p2p_id DESC LIMIT %d
			";
		$result = $wpdb->get_results($wpdb->prepare("$querystr", $type, $meta_value, $meta_value, $limit), OBJECT);
	    
	    
		return $result;
    }
}
/* end this function is only used on the bill moyers site */



// delete p2p relationships when a post is trashed 
if ( ! function_exists('wt_delete_p2p_on_trash') ) {
  function wt_delete_p2p_on_trash() {
    global $post;
    $post_id = $post->ID;
    if ($post_id) {
      if (function_exists('p2p_register_connection_type') ) {
        global $wpdb;
        $statement = $wpdb->prepare("DELETE FROM $wpdb->p2p WHERE p2p_to = %d OR p2p_from = %d", $post_id, $post_id); 
        $wpdb->query($statement);
      }
    } 
  }
}
add_action('wp_trash_post', 'wt_delete_p2p_on_trash');


/* END of FILE */
