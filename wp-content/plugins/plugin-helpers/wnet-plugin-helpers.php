<?php 

/*
Plugin Name: Plugin Helpers
Plugin URI: 
Description: Replaces default queries for zoninator, posts-to-posts. Also includes misc shared functions.
Author: 
Version: 1.0
Author URI:
*/


/**
 * Detect plugin. For use on Front End only.
 * http://codex.wordpress.org/Function_Reference/is_plugin_active
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );


if ( is_plugin_active( 'zoninator/zoninator.php' ) ) {
	require_once('_zoninator.php');
	}


if ( is_plugin_active( 'posts-to-posts/posts-to-posts.php' ) ) {
	require_once('_posts_to_posts.php');
}

if ( is_plugin_active( 'wordpress-seo/wp-seo.php' ) ) {
	// this removes yoast from the wordpress toolbar.
	require_once('_yoast_seo.php');
}


require_once('_disable_heartbeat.php');



/* misc common helpers */
require_once('helpers/_cove-shortcode.php');
require_once('helpers/_wnet-get-image-url.php');
require_once('helpers/_wnet-get-permalink.php');
require_once('helpers/_wnet-truncate.php');
require_once('helpers/_wnet-excerpt-by-id.php');
require_once('helpers/_wnet-paging-nav.php');
require_once('helpers/_wnet-share-bar.php');
require_once('helpers/_wnet-ngg-gallery-replacement.php');
require_once('helpers/_wnet-popular-posts.php'); /* jetpack */
require_once('helpers/_wnet_ajax_endpoint.php'); /* ajax endpoint */
require_once('helpers/_wnet-page-ancestor.php'); /* get page ancestor page id */
require_once('helpers/_wnet-uplynk-shortcode.php'); /* uplynk player shortcode */
require_once('helpers/_wnet-soundcloud-shortcode.php'); /* soundcloud player shortcode */

/* end of file */