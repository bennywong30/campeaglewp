<?php 

add_action( 'wp_before_admin_bar_render', 'remove_yoast_from_toolbar');

function remove_yoast_from_toolbar(){
	global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wpseo-menu');
}

/* END of FILE */