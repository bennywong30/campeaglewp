<?php 
/* get the pages top level ancestor page id */

if ( !function_exists('wnet_page_ancestor')) {
function wnet_page_ancestor($pid) {
	$parents = get_post_ancestors( $pid );
	$id = ($parents) ? $parents[count($parents)-1]: $pid;
	return $id;
}
}

/* END of FILE */