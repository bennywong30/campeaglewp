<?php 
/* 
Sets a transient so we dont keep checking the popular posts!
Note: We have to loop through a lot of extra posts to find posts with vimeo AND specific post types. 
We do however kill the function and save a transient when we find enough posts to fill the MAX count.
*/

if ( !function_exists('wnet_popular_posts')) {
function wnet_popular_posts($args = array()) {
if ( function_exists('stats_get_csv') ) {

	$defaults = array(
		'transient' => 'wnet-popular-posts', 
		'max' => 12, 
		'days' => 5, 
		'limit' => 50, 
		'custom_field' => '',
		'post_type' => array('post')
	);
	$args = array_merge($defaults, $args);

	$transient = $args['transient'];
	$max = $args['max'];
	$days = $args['days'];
	$limit = $args['limit'];
	$custom_field = $args['custom_field'];
	$post_type = $args['post_type'];
	
	
	if ( false === ( $popular = get_transient($transient) ) ) {	
		$popular = array();
		$posts = stats_get_csv('postviews', "days=". $days ."&limit=".$limit."");
		$cnt = 0;

		foreach($posts as $p) {
			$post = get_post($p['post_id']);
			$customcheck = get_post_meta($post->ID, $custom_field, true);
			
			if ('0' < $post->ID) {
				if (in_array($post->post_type, $post_type)) {
					// if custom field check is passed
					if ($custom_field != "" && $customcheck != "") {
						array_push($popular, $post->ID);
						$cnt++;
					}
					// else just check post types.
					elseif ($custom_field == "") {
						array_push($popular, $post->ID); 
						$cnt++;
					}
				}	
			} 
			if ($cnt == $max) { break; }
		}
		// set long transient.
		set_transient($transient, $popular, 3600);
	}
	return $popular;
	}
}
}
/* END of FILE */