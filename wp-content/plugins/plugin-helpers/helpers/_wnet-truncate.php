<?php 

/*  use wnet_truncate on fields when I can only display X characters
  * example truncate dek on carousel to 200 chars
  * Usage: wnet_truncate($content, 90);
*/ 
if ( ! function_exists('wnet_truncate'))
{
   function wnet_truncate($text, $length, $stripTags=true) {
         $length = abs((int)$length);
		 $text = preg_replace('/<a href=\"(.*?)\">(.*?)<\/a>/', "", $text);
		 if ($stripTags == true) { $text = strip_tags($text);}
         if(strlen($text) > $length) {$text = preg_replace("/^(.{1,$length})(\s.*|$)/s", '\\1 ...', $text);}
         return($text);
   }
}

/* END of FILE */