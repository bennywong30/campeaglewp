<?php 

/* note: these were in the santalone-wpmu plugin, theyre in use in some of the older sites. all wrapped in checks to avoid conflicts.


/* INLINE HIDDEN THICKBOX DIV */
if ( !function_exists('legacy_InlineThickBox')) {
function legacy_InlineThickBox($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"title" => '',
		"style" => 'display:none'
	), $atts));
	return '<div id="'.$id.'" style="'.$style.'">'.$content.'</div>';
}
   add_shortcode("tb", "legacy_InlineThickBox");  
}   
/* END INLINE HIDDEN THICKBOX DIV */



/* IFRAME */
if ( !function_exists('legacy_UIframe')) {
function legacy_UIframe($atts, $content = null) {
	extract(shortcode_atts(array(
		"width" => '630',
		"height" => '100',
		"src" => ''
	), $atts));
	return '<iframe src="'.$src.'" width="'.$width.'" height="'.$height.'" frameborder="0"  scrolling="no"></iframe><p></p>';
}
   add_shortcode("MYiframe", "legacy_UIframe");  
}   
/* END IFRAME */



/* MEDIA ELEMENT */   
if ( !function_exists('legacy_mediaelementEmbed')) {
function legacy_mediaelementEmbed($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => 'player2',
		"src" => '',
		"caption" => '',
		"height" => '295'
	), $atts));
	return '<div style="margin: 0 auto 10px auto; width: 400px; font: normal 11px arial; color: #666;"><audio id="'.$id.'" src="'.$src.'" type="audio/mp3" controls="controls"></audio><p style="margin: 4px 0 0 0">'.$caption.'</p></div><script>jQuery("audio,video").mediaelementplayer();</script>';
}
add_shortcode("mediaelement", "legacy_mediaelementEmbed");     
} 
/* END MEDIA ELEMENT */  



/* COMMENTED OUT TAGS */
if ( !function_exists('legacy_commentedOut')) {
function legacy_commentedOut($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => ''
	), $atts));
	return '<!-- '.$content.' -->';
}
   add_shortcode("hidden", "legacy_commentedOut"); 
}     
/* END COMMENTED OUT TAGS */   	



/* pullquote */
if ( !function_exists('legacy_THIRTEENPullQuote')) {
function legacy_THIRTEENPullQuote($atts, $content = null) {
 extract(shortcode_atts(array(
  "align" => ''
 ), $atts));
 return '<pullquote class="pullquote align'.$align.'">'.$content.'</pullquote>';
}
add_shortcode("pullq", "legacy_THIRTEENPullQuote");    
}
/* END pullquote */





/* END of FILE */	