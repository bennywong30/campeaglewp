<?php 

 /* Custom sharebar */

if ( !function_exists('wnet_share_bar')) {
	function wnet_share_bar($pid) {

	if (function_exists('wnet_get_permalink')) {$permalink = wnet_get_permalink($pid);}
	else {$permalink = get_permalink($pid);}
	$title = get_the_title($pid);
	
	echo "<ul class='addthis_toolbox newtoolbar cf' addthis:url='$permalink' addthis:title=\"". $title ."\">
<li class='addthis_counter' style='display: none;'></li>
	<li><a href='#' class='addthis_button_facebook'><span class='ico'><i class='fa fa-facebook'></i><span class='label'>Like</span><span class='cnt cnt-facebook'></span></span></a></li>
	<li><a href='#' class='addthis_button_twitter'><span class='ico'><i class='fa fa-twitter'></i><span class='label'>Tweet</span><span class='cnt cnt-twitter'></span></span></a></li>
	<li><a href='#' class='addthis_button_compact'><span class='ico'><i class='fa fa-plus'></i><span class='label'>Share</span><span class='cnt cnt-addthis'></span></span></a></li>
	<li><a href='#' class='addthis_button_email'><span class='ico'><i class='fa fa-envelope'></i><span class='label'>Email</span></span></a></li>
</ul>";
}
}

/* END of FILE */
