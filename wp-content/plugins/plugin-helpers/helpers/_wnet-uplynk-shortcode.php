<?php

/* uplynk player shortcode */
if ( !function_exists('wnet_uplynk_shortcode')) {
function wnet_uplynk_shortcode($atts, $content = null) {
	extract(shortcode_atts(array("id" => ''), $atts));
	return '<div class="video-wrap"><iframe src="https://content.uplynk.com/player/'.$id.'.html" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen width="512" height="288"></iframe></div>';
}
   add_shortcode("wnet-uplynk", "wnet_uplynk_shortcode");  
}

/* end uplynk player shortcode */
