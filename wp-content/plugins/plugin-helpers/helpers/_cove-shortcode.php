<?php 

// cove video shortcode.
if ( !function_exists('wnet_cove_shortcode')) {
function wnet_cove_shortcode($atts, $content = null) {
 extract(shortcode_atts(array(
   "id" => '',
   "topbar" => 'true',
   "button" => 'false',
   "postid" => '',
   
 ), $atts));

 $buttonCode = "";
 $passed = 0;
 
if ($button == 'true') {
	global $post;
	if ($postid != '') {
		$checkpost = $postid;
		$passed = 1;
	}
	else {
		$checkpost = $post->ID;
		$passed = 0;
	}
	
 	if (function_exists('wnet_np_full_episode_available')) {$buttonCode = wnet_np_full_episode_available($checkpost, 'shortcodebutton', $passed);}
}
else {$buttonCode = "";}


if ($topbar == 'true') {$wrapClass = 'has-topbar';}
else {$wrapClass = 'no-topbar';}


return '<div class="video-wrap '.$wrapClass.' cf">'. $buttonCode .'<iframe id="partnerPlayer" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"  src="http://player.pbs.org/widget/partnerplayer/'.$id.'/?chapterbar=false&topbar='.$topbar.'&autoplay=false" allowfullscreen></iframe></div>';
}
add_shortcode("wnet-cove", "wnet_cove_shortcode");  
}

/* END of FILE */