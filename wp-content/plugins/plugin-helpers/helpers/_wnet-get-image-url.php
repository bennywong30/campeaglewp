<?php 

/* get thumbnail URL */
if ( !function_exists('wnet_get_image_url')) {
   function wnet_get_image_url($pid, $args = array()) {
   
   $defaults = array('w' => '480', 'h' => '270', 'default' => '', 'default_path' => '', 'customkey' => '', 'use_mezz' => false, 'mezz_key' => 'video_image');
   $args = array_merge($defaults, $args);
   
   $w = $args['w'];
   $h = $args['h'];
   $default = $args['default'];
   $default_path = $args['default_path'];
   $customkey = $args['customkey'];


	if ($default_path != "") {$imgPath = $default_path;}
	else {
		$styleDir = get_bloginfo('stylesheet_directory');
		$imgPath = "$styleDir/libs/images/";	
	}

	if ($args['use_mezz'] == 1 && get_post_meta($pid, $args['mezz_key']) && function_exists("wnet_cove_mezz_resize")) {
		$thumbnail = get_post_meta($pid, $args['mezz_key'], true);
		
		if ($args['mezz_key'] == '_program_video_thumb') {
			// this if statement is used in the local hp grid item posts, it will be phased out... 
			$thumbnail = str_replace(".resize.1400x788.jpg", "", $thumbnail);
			$thumbnail = str_replace(".resize.1400x788.png", "", $thumbnail);
		}
		
		$thumbnail = wnet_cove_mezz_resize($thumbnail, $w, $h);
	}

	elseif ( $customkey != "" && get_post_meta($pid, $customkey, true) != "") {
		$thumbnail = get_post_meta($pid, $customkey, true);
	}

	elseif ( function_exists('has_post_thumbnail') && has_post_thumbnail($pid) ) {
		$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($pid), array($w,$h));
		$thumbnail = $thumbnail[0];
	}

	// else defaut img 
	else if ($default != "") {$thumbnail = "$imgPath$default";}

	return $thumbnail;
}
}

/* END of FILE */