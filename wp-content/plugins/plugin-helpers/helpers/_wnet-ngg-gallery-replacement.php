<?php
/*
 * wnet_ngg_query replacing the default NGG for slideshow frontend
 */

if ( ! function_exists('wnet_ngg_query'))
{
    function wnet_ngg_query($post_id, $limit) {
	
		global $wpdb;

	    $wpdb->ngg_pictures = $wpdb->prefix . 'ngg_pictures';
	    $wpdb->ngg_gallery = $wpdb->prefix . 'ngg_gallery';
		$wpdb->postmeta = $wpdb->prefix . 'postmeta';
	    $wpdb->posts = $wpdb->prefix . 'posts';	   

            $querystr = "SELECT ng.author, ng.title,
            np.pid, np.galleryid, np.post_id, ng.path, np.image_slug, np.filename,
            np.description, np.alttext, np.imagedate
            FROM $wpdb->ngg_pictures AS np
            INNER JOIN $wpdb->ngg_gallery AS ng ON ng.gid = np.galleryid
	    INNER JOIN $wpdb->postmeta AS pp ON pp.meta_value = np.pid AND pp.meta_key = 'ngggallery'
            INNER JOIN $wpdb->posts AS p ON pp.post_id = p.ID
            WHERE pp.post_id = %d AND p.post_status = 'publish'
            ORDER BY np.pid * 1
            LIMIT %d";
            $result = $wpdb->get_results($wpdb->prepare($querystr, $post_id, $limit), OBJECT);
			
            return $result;
    }
}

function wnet_ngg_shortcode_exists( $shortcode = false ) {
	global $shortcode_tags;
	if ( ! $shortcode )
		return false;
 	if ( array_key_exists( $shortcode, $shortcode_tags ) )
		return true;
 	return false;
}

function wnet_ngg_replace_shortcode() {
	$shortcode = 'nggallery';
	if( wnet_ngg_shortcode_exists( $shortcode ) ) {
		// nextgen is active, so use legacy nextgen function
	} else {
		// nextgen is not active, lets use benny's funtion.
		add_shortcode("nggallery", "wnet_nggallery");  
	}
}

// NEW nextgen shortcode replacement.
if (!function_exists('wnet_nggallery')) {
function wnet_nggallery($atts, $content = null) {
 extract(shortcode_atts(array(
   "id" => '',
   "items" => '',
    "count" => '30'
 ), $atts));
	global $post;
	
	$gallery = wnet_ngg_query($post->ID, $count);

	foreach ($gallery as $photo) {
		$home = get_bloginfo('home');
		$path = $photo->path;
		$thumbnail = $photo->filename;
		$caption = $photo->description;
		$caption = str_replace('"', "'", $caption);
		$caption = stripslashes($caption);
	
		$items .= "<dl class='gallery-item'><dt class='gallery-icon landscape'><a href='$home/$path/$thumbnail' rel='gallery-$post->ID' class='thickbox'  title=\"" . $caption. "\" ><img width='130' height='119' src='$home/$path/thumbs/thumbs_$thumbnail' data-lazy-src='$home/$path/thumbs/thumbs_$thumbnail' class='attachment-thumbnail' alt='' /></a></dt></dl>";
	}
	if ($items != "") { return "<div id='gallery-$id' class='gallery galleryid-$post->ID gallery-columns-4 gallery-size-thumbnail cf'>$items</div>";}

}
wnet_ngg_replace_shortcode();
}
/* END nextgen shortcode replacement */

/* END of FILE */
