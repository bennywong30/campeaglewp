<?php

/* soundcloud shortcode */
if (!shortcode_exists( 'soundcloud' ) && !function_exists('wnet_soundcloud_shortcode')) {
function wnet_soundcloud_shortcode($atts, $content = null) {
	extract(shortcode_atts(
		array(
		"url" => '',
		"width" => '100%',
		"height" => '166',
		), $atts));
	return '<div class="soundcloud-wrap"><iframe width="'.$width.'" height="'.$height.'" scrolling="no" frameborder="no" src="//w.soundcloud.com/player/?url='.$url.'&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe></div>';
}
   add_shortcode("soundcloud", "wnet_soundcloud_shortcode");  
}

/* end soundcloud shortcode */