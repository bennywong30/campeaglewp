<?php
/*
WNET AJAX API Endpoint
Adds an AJAX API endpoint at /api/ajax/?template=template-name
Templates must live at /themes/views/ajax/template-name.php

V.2.0
Improved security by sanitizing vars and checking for templates.
*/



class wnetajax_API_Endpoint{
	
	public function __construct(){
		add_filter('query_vars', array($this, 'add_query_vars'), 0);
		add_action('parse_request', array($this, 'sniff_requests'), 0);
		add_action('init', array($this, 'add_endpoint'), 0);
	}	
	
	public function add_query_vars($vars){
		$vars[] = '__api';
		$vars[] = 'template';
		return $vars;
	}
	
	public function add_endpoint(){
		add_rewrite_rule('^api/ajax/?([0-9]+)?/?','index.php?__api=1&template=$matches[1]','top');
	}
	
	public function sniff_requests(){
		global $wp;
		if(isset($wp->query_vars['__api'])){
			$this->handle_request();
			exit;
		}
	}
	
	protected function handle_request(){
		global $wp;
		
		/*
		BS: sanitize the template variable, hackers like to pass file paths here, this will clean those up.
		then we'll also limit the number of chanracters returned.
		*/
		$templateVar = $wp->query_vars['template'];
		
		if (strlen($templateVar) > 35) {
			// die if long variable passed, probably a hacking attempt.
			echo "<!DOCTYPE html><h1>Error 400</h1>Bad request.";
			http_response_code(400);
			die();
		}
		$templateVar = sanitize_title($templateVar);
		if (strlen($templateVar) > 35) {$templateVar = substr($templateVar, 0, 35);}

		if (!empty($templateVar)){
    		$ajaxTemplate = locate_template(array('views/ajax/' . $templateVar . '.php'), false, false);
		   	if ($ajaxTemplate) {
				// if ajax template found.
				$template = TEMPLATEPATH . '/views/ajax/' . $templateVar . '.php'; 
				if($template) {$this->send_response('200 OK', $template);}
			}
		   	else {
				// bad template var was passed because we couldnt find the template file in the theme.
				$this->send_response('<h1>Error 404</h1>Error locating template.', '', '404');
			}
		}
		else {
			// no template var was passed.
			$this->send_response('<h1>Error</h1>Output template not specified.', '', '404');
		}
	}

	protected function send_response($msg, $template = '', $error=''){
		if($template) {
			echo "<!DOCTYPE html>"; 
			include ($template); 
			}
		else {
			echo "<!DOCTYPE html>";
			echo $msg;
			if (!empty($error)) {http_response_code($error);}
		}
	    exit;
	}
}
new wnetajax_API_Endpoint();