<?php 

/* get permalink, checks for redirect url */
if ( !function_exists('wnet_get_permalink')) {
   function wnet_get_permalink($pid) {
   	if ("" != get_post_meta($pid, '_redirect', true)) {$permalink = get_post_meta($pid, '_redirect', true);}
	else if ("" != get_post_meta($pid, 'redirect', true)) {$permalink = get_post_meta($pid, 'redirect', true);}
	else {$permalink = get_permalink($pid);}
	return $permalink;
}
}

/* END of FILE */