<?php 
/* get the post excerpt by id */

if ( !function_exists('wnet_excerpt_by_id')) {
function wnet_excerpt_by_id($pid, $excerpt_length = 75, $morelink=NULL) {
	$link = "";
	$the_post = get_post($pid);
	$the_excerpt = $the_post->post_excerpt;
	
	if ($the_post->post_excerpt == "") {$the_excerpt = $the_post->post_content;}
	else {$the_excerpt = $the_post->post_excerpt;}
		
	$the_excerpt = strip_tags(strip_shortcodes($the_excerpt));
	$words = explode(' ', $the_excerpt, $excerpt_length + 1);

	if(count($words) > $excerpt_length) :
		array_pop($words);
		array_push($words, '...');
		$the_excerpt = implode(' ', $words);
	endif;
	
	if ($morelink != "") {
		$permalink = wnet_get_permalink($pid);
		$link = " <a href='$permalink' class='more-link'>$morelink</a>";
	}
	
	$the_excerpt = '<p>' . $the_excerpt . '' . $link . '</p>';
	
	return $the_excerpt;
}
}

/* END of FILE */