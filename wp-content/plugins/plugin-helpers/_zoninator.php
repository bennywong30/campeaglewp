<?php 


/* Replace the default zoninator query with this OPTIMIZED one */
if ( ! function_exists('wnet_get_posts_in_zone')) {
	function wnet_get_posts_in_zone($zone_name, $limit = 1){
		global $wpdb;
		$zone_id = bw_get_slug_id($zone_name);
		$querystr = sprintf("SELECT $wpdb->postmeta.post_id AS ID FROM $wpdb->postmeta WHERE $wpdb->postmeta.meta_key = '_zoninator_order_%d' ORDER BY $wpdb->postmeta.meta_value+0 ASC LIMIT %d", $zone_id, $limit);
		$result = $wpdb->get_results($wpdb->prepare("$querystr",NULL), OBJECT);
		return $result;
	}
}

if ( ! function_exists('bw_get_slug_id')) {
	function bw_get_slug_id($slug){
		global $wpdb;
		$querystr = sprintf("SELECT $wpdb->terms.term_id FROM $wpdb->terms WHERE $wpdb->terms.slug = '%s' LIMIT 1", $slug);
		$result = $wpdb->get_var($wpdb->prepare("$querystr",NULL));
		return $result;
	}
}
/* END Replace the default zoninator query with this OPTIMIZED one */




/* END of FILE */
