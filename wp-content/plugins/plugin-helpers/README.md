# PLUGIN QUERY REPLACEMENTS AND HELPER FUNCTIONS #
WNET Plugin Helpers contains a default set of queries and helper functions that we use across our sites both locally and nationally. 






## HELPER FUNCTIONS ##
---

### WordPress Heartbeat ###
The WordPress HeartBeat is disabled by this plugin.

---
### WNET Posts 2 Posts Queries ###
New custom queries for "Posts 2 Posts"

```
#!php

BS to document the p2p queries here.
```

---
### WNET Get Posts In Zone ###
Lightweight query for "Zone Manager" written by Benny.

Usage: call function wnet_get_posts_in_zone($zone_name, $maxResults).

```
#!php
if (function_exists('wnet_get_posts_in_zone')) {
$posts = wnet_get_posts_in_zone($zone_name, 5);
}
```

---
### WNET COVE Shortcode ###
Usage: place shortcode in post body.
```
#!php
[wnet-cove id='coveIDhere']
```

---
### WNET Get Image URL ###
Usage: call function wnet_get_image_url($post->ID, $args).

Pass args as array, available args:

* "w" = width of image.
* "h" = height of image.
* "default" = default image name ex: default.jpg. Must live at /theme/libs/images/, else pass default path. This can be set as blank if you don't want an image returned.
* "default_path" = optional, if you need to change path for default image.
* "customkey" = name of custom field which could contain image override.


```
#!php
if (function_exists('wnet_get_image_url')) {
$args = array('w' => '480', 'h' => '270', 'default' => '', 'default_path' => '', 'customkey' => '');
wnet_get_image_url($post->ID, $args);
}

```



---
### WNET Get Permalink ###
Usage: call function wnet_get_permalink($post->ID).

If meta key "_redirect" is set, permalink will use redirect. Else will use standard get_permalink() function to generate link.

```
#!php

if (function_exists('wnet_get_permalink')) {
   $permalink = wnet_get_permalink($post->ID);
}
```

---
### WNET Truncate ###
Usage: call function wnet_truncate($text, 100). 

Pass the text you'd like to truncate, and the desired number of characters. Tags will be stripped before truncation occurs.

```
#!php
if (function_exists('wnet_truncate')) {
echo wnet_truncate($text, 30);
}
```

---
### WNET Excerpt By ID ###
Usage: call function wnet_excerpt_by_id($post->ID, 100, 'Read More').  

Read more link is option, omit field if not necessary.

```
#!php
if (function_exists('wnet_excerpt_by_id')) {
echo wnet_excerpt_by_id($post->ID, 75);
}

```

---
### WNET Paging Nav ###
Usage: call function wnet_paging_nav($classname);

Generates pagination links for archive page. 

This replaces functionality that we used to rely on the wp-pagnavi plugin for.


```
#!php
if (function_exists( 'wnet_paging_nav' ) ) {
wnet_paging_nav('paging-navigation');
}

```


---
### WNET Popular Posts ###
Usage: call function wnet_popular_posts($args).
Pass args as array, available args:

* "transient" = name of transient to set defaults to 'wnet-popular-posts'.
* "max" = number of items you want to return.
* "days" = num of days to search. set as integer 1-30.
* "limit" = defaults to (50). Set to lowest possible number to get result set you need. Sometimes you need a larger number to get to the posts you want.
* "custom_field" = *optional, set custom field that post must have value filed out. Example: 'video', only posts with 'video' custom field will be returned.
* "post_type" = pass array of post types to include. Default is just the standard post type. "array('post')".


```
#!php
if (function_exists('wnet_popular_posts')) {
$args = array('transient' => 'wnet-popular-posts', 'max' => 12, 'days' => 5, 'limit' => 50, 'custom_field' => '', 'post_type' => array('post'));
$posts = wnet_popular_posts($args);
}

```




---
### WNET Share Bar ###
Usage: call function wnet_share_bar($post->ID).

Function prints a sharing toolbar which is integrated with addThis. 

Sharing buttons are generated for facebook, twitter, google+, and email. 

Requires addThis JS in page footer, and Font Awesome CSS for icons.


```
#!php

if (function_exists('wnet_share_bar')) {
 wnet_share_bar($post->ID);
}
```



---
### WNET Station Data ###
Usage: call function wnet_get_station_data(). See helper file for args to pass to function.

Function returns array of schedule data for our local stations.


```
#!php
if (function_exists('wnet_get_station_data')) {
$data = wnet_get_station_data( $limit=3, $type, $schedule_station=NULL, $program_id=NULL, $search=NULL, $search_days=3, $genres=NULL );
}
```


---
### WNET NextGen Gallery Replacement ###
Replaces the default functionality of the [nggallery] shortcode when the nextgen plugin is deactivated. We shouldn't have to use this very much as we're never going to use that plugin again! This just allows for legacy galleries to be displayed on the front end.

```
#!php
[nggallery]

```